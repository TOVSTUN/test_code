<?php
/* @var $this yii\web\View */
$this->title = Yii::t('app', 'Зміна пароля');

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Панель керування'), 'url' => ['/shop/dashboard']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="language-update">

    <?= /** @var \frontend\controllers\SettingsAdminController actionProfile() $model */
    $this->render('_password_form', [
        'model' => $model,
    ]) ?>

</div>
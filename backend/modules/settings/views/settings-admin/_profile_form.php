<?php

use backend\modules\translate\models\TranslateSupport;
use core\entities\Language;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var core\entities\User $model */
/* @var $this yii\web\View */
?>
<div class="row">
    <div class="form-group col-md-5">
        <div class="language-form">

            <?php $form = ActiveForm::begin(); ?>

            <?=
            $form->field($model, 'username')
                ->textInput(['maxlength' => true])
                ->label(Yii::t('app', 'Имя пользователя'));

            ?>

            <?= $form->field($model, 'email')
                ->textInput(['maxlength' => true, 'type' => 'email'])
                ->label(Yii::t('app', 'email'));
            ?>
            <?= $form->field($model, 'language_id')
                ->textInput(['autofocus' => true])
                ->dropDownList(Language::getArrayIdName(), ['options' => [Yii::$app->user->identity->language_id => ['Selected' => true]]])
                ->label(Yii::t('app', 'Мова інтерфейсу'));
            ?>



            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

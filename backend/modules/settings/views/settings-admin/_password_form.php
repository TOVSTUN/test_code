<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var core\entities\User $model */
/* @var $this yii\web\View */
?>
<div class="row">
    <div class="form-group col-md-5">
        <div class="language-form">

            <?php $form = ActiveForm::begin(['id'                   => 'password-form',
                                             'enableAjaxValidation' => true]); ?>

            <?=
            $form->field($model, 'oldPassword')
                ->textInput(['maxlength' => true, 'type' => 'password'])
            ?>

            <?= $form->field($model, 'newPassword')
                ->textInput(['maxlength' => true, 'type' => 'password'])
            ?>

            <?= $form->field($model, 'newPassword2')
                ->textInput(['maxlength' => true, 'type' => 'password'])
            ?>

            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

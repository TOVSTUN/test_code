<?php
/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = Yii::t('app', 'Налаштування особових даних');

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Панель керування'), 'url' => ['/shop/dashboard']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="language-update">

    <?= /** @var \frontend\controllers\SettingsAdminController actionProfile() $model */
    $this->render('_profile_form', [
        'model' => $model,
    ]) ?>

</div>

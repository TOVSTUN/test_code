<?php
/**
 * Created by PhpStorm.
 * User: tovstun
 * Date: 06.02.18
 * Time: 10:18
 */


use backend\modules\settings\MenuSettingsHelper;
use yii\helpers\Html;

/** @var common\entities\Menu $menu */
/* @var $this yii\web\View */


$this->params['url'] = 'niestable_list';
$this->title = $menu->name;

MenuSettingsHelper::showBreadctumbs($this);

?>


<div class="row">
    <div class="col-md-12">
        <div id="nestable-menu">

            <?= Html::a(Yii::t('app', 'додай новий пункт меню'),
                ['/settings/menu-item/create?menu_id=' . $menu->id],
                ['class' => 'btn btn-success btn-sm my-modal-lg']) ?>

            <?= Html::a(Yii::t('app', 'скоректуй меню'),
                ['update?id=' . $menu->id],
                ['class' => 'btn btn-primary btn-sm  my-modal-lg left-200']) ?>


            <button type="button" data-action="expand-all"
                    class="btn btn-white btn-sm"><?= Yii::t('app', 'Expand All') ?></button>
            <button type="button" data-action="collapse-all"
                    class="btn btn-white btn-sm"><?= Yii::t('app', 'Collapse All') ?></button>
        </div>
    </div>
</div>

<div class="row">


    <div class="col-lg-6 list-1">
        <div class="ibox">
            <div class="ibox-title">
                <h5>Конструктор меню для - <?= $menu->name; ?> </h5>
            </div>
            <div class="ibox-content ">

                <p class="m-b-lg">
                    <strong>Nestable</strong>

                </p>

                <div class="dd" id="nestable">

                    <?= (new \backend\modules\settings\models\ConstructorMenu())->getConstructorMenu($menu); ?>

                </div>

                <textarea id="nestable-output" class="form-control hide"></textarea>

            </div>
        </div>
    </div>

    <div class="col-lg-6 list-2">

        <div class="ibox ">
            <div class="ibox-title">
                <h5>Глобальні пункти меню</h5>
            </div>
            <div class="ibox-content">

                <p class="m-b-lg">
                    Увага! Тут виводяться глобальні пункти меню, які можуть бути підключені одночасно до різних блоків
                    меню.
                </p>

                <?= (new \backend\modules\settings\models\ConstructorMenu())->getGlobalItemList(); ?>

            </div>

        </div>
    </div>
</div>


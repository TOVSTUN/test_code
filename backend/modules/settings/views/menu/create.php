<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model core\entities\Menu */

$this->title = 'Створи нове Menu';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Menus'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menu-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

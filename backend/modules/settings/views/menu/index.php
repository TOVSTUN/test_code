<?php

use backend\modules\settings\MenuSettingsHelper;
use common\components\MinorHelper;
use common\statuses\MyStatus;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\entities\search\MenuSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Налаштування Меню');
MenuSettingsHelper::showBreadctumbs($this);

?>
<div class="menu-index">


    <p>
        <?= Html::a('Створи нове меню', ['create'], ['class' => 'btn btn-success my-modal-lg']) ?>
    </p>
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'layout'       => "{pager}\n{items}",
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'label'  => Yii::t('app', 'Меню'),
                'format' => 'raw',
                'value'  => function($data){
                    return Html::a($data->name, '/settings/menu/config?id=' . $data->id,
                        [
                            'title' => Yii::t('app', 'Сформувати меню'),
                            // 'target' => '_blank',

                        ]);
                },
            ],

            [
                'attribute'      => 'role_id',
                'contentOptions' => ['style' => 'width: 100px; max-width: 100px;'],
                'value'          => function($data){
                    return MinorHelper::getUserRole($data->role_id);
                },
                'label'          => 'Role',

            ],

            [
                'attribute'      => 'system_name',
                'label'          => 'Cистемна назва',
                'contentOptions' => ['style' => 'width: 150px; max-width: 150px;'],

            ],
            [
                'attribute'      => 'name_visibility',
                'value'          => function($data){
                    return MyStatus::whatStatus($data->name_visibility);
                },
                'label'          => 'Показувати назву',
                'contentOptions' => ['style' => 'width: 150px; max-width: 150px;'],


            ],
            [
                'attribute'      => 'item_name_visibility',
                'value'          => function($data){
                    return MyStatus::whatStatus($data->item_name_visibility);
                },
                'label'          => Yii::t('app', 'Назви пунктів меню'),
                'contentOptions' => ['style' => 'width: 200px; max-width: 200px;'],
            ],
            [
                'attribute'      => 'item_icon_visibility',
                'value'          => function($data){
                    return MyStatus::whatStatus($data->item_icon_visibility);
                },
                'label'          => Yii::t('app', 'Показувати іконки'),
                'contentOptions' => ['style' => 'width: 200px; max-width: 200px;'],

            ],

            [
                'attribute' => 'status',
                'value'     => function($data){
                    return MyStatus::whatStatus($data->status);
                },
                'filter'    => MyStatus::onOff(),
            ],

            ['class' => 'yii\grid\ActionColumn', 'buttons' => ['update' => function($url, $model){
                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['class' => 'my-modal-lg', 'data-pjax' => '0']);
            }, 'view'                                                   => function($url, $model){
                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['class' => 'my-modal-lg', 'data-pjax' => '0']);
            },],],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>

<?php

use common\components\MinorHelper;

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\statuses\MyStatus;

/* @var $this yii\web\View */
/* @var $model common\entities\Menu */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="menu-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form
        ->field($model, 'name')
        ->textInput()
        ->label('Назва меню')
    ?>

    <?= $form->field($model, 'description')->textarea() ?>

    <?= $form->field($model, 'system_name')
        ->textInput(['maxlength' => true])
        ->dropDownList(['left_menu' => 'left_menu', 'top_menu' => 'top_menu']) ?>

    <?= $form->field($model, 'status')->textInput()->dropDownList(MyStatus::onOff()) ?>

    <?= $form->field($model, 'name_visibility')->textInput()->dropDownList(MyStatus::onOff())->label('Показувати назву') ?>

    <?= $form->field($model, 'item_name_visibility')->textInput()->dropDownList(MyStatus::onOff())->label('Відображати назви пунктів меню') ?>

    <?= $form->field($model, 'item_icon_visibility')->textInput()->dropDownList(MyStatus::onOff())->label('Показувати іконки') ?>

    <?php
    echo $form->field($model, 'role_id')
        ->textInput()
        ->dropDownList(\common\entities\Role::getAllRoles())
        ->label('Для кого це меню')
    ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

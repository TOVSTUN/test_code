<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model core\entities\search\MenueSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="menu-search">

    <?php $form = ActiveForm::begin([
        'action'  => ['index'],
        'method'  => 'get',
        'options' => [
            'data-pjax' => 1,
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'system_name') ?>

    <?= $form->field($model, 'status') ?>

    <?= $form->field($model, 'order') ?>

    <?= $form->field($model, 'name_visibility') ?>

    <?php // echo $form->field($model, 'items_string') ?>

    <?php // echo $form->field($model, 'item_name_visibility') ?>

    <?php // echo $form->field($model, 'item_icon_visibility') ?>

    <?php // echo $form->field($model, 'old_items_string') ?>

    <?php // echo $form->field($model, 'old_items_string_update') ?>

    <?php // echo $form->field($model, 'user_role_id') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use backend\modules\settings\MenuSettingsHelper;
use backend\modules\settings\widgets\Breadcrumbs;
use common\statuses\MyStatus;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\entities\search\MenuItemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Налаштування пунктів меню');

MenuSettingsHelper::showBreadctumbs($this);

?>
<div class="menu-item-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Додай новий пункт меню'), ['create'], ['class' => 'btn btn-success my-modal-lg']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn', 'contentOptions' => ['style' => 'width: 50px; max-width: 50px;']],
            [
                'attribute'      => 'name',
                'contentOptions' => ['style' => 'width: 150px; max-width: 150px;'],
            ],
            [
                'attribute'      => 'url',
                'contentOptions' => ['style' => 'width: 300px; max-width: 300px;'],
                'format'         => 'html',
                'value'          => function($data){
                    return "<a href='$data->url'>$data->url</a>";
                },
            ],
            [
                'attribute'      => 'icon',
                'contentOptions' => ['style' => 'width: 120px; max-width: 120px;'],
            ],
            [
                'attribute'      => 'global',
                'contentOptions' => ['style' => 'width: 120px; max-width: 120px;'],

                'value'  => function($data){
                    return MyStatus::whatJak($data->global);
                },
                'filter' => MyStatus::onOff(),
            ],
            [
                'attribute'      => 'status',
                'contentOptions' => ['style' => 'width: 120px; max-width: 120px;'],

                'value'  => function($data){
                    return MyStatus::whatStatus($data->status);
                },
                'filter' => MyStatus::onOff(),
            ],


            ['class'   => 'yii\grid\ActionColumn',
             'buttons' => [
                 'update' => function($url, $model){
                     return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url,
                         ['class' => 'my-modal-lg', 'data-pjax' => '0']);
                 },
                 'view'   => function($url, $model){
                     return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url,
                         ['class' => 'my-modal-lg', 'data-pjax' => '0']);
                 },
             ],
            ],
        ],
    ]); ?>
</div>

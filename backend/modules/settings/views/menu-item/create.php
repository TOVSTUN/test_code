<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\entities\MenuItem */

$this->title = Yii::t('app', 'Новий пункт');

?>
<div class="menu-item-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

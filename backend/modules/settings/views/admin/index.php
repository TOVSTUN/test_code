<?php

use common\statuses\MyStatus;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\entities\search\AdminSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Administratorzy systemy');
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admin-index">


    <p>
        <?= Html::a('Dodaj nowego administratora', ['create'], ['class' => 'btn btn-success my-modal-md']) ?>
        <?= Html::a('Usuń filtry', ['/admin'], ['class' => 'btn btn-sm btn-info']) ?>


    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn', 'contentOptions' => ['style' => 'width: 50px; max-width: 50px;'],],

            [
                'attribute'      => 'username',
                'contentOptions' => ['style' => 'width: 150px; max-width: 100px;'],
                'format'         => 'raw',
                'value'          => function($data){

                    return Html::a($data->username,
                        ['admin/config-access', 'admin_id' => $data->id],
                        ['class' => 'my-modal-lg']);
                },
                'label'          => 'Nazwa',
            ],
            [
                'contentOptions' => ['style' => 'width: 150px; max-width: 100px;'],
                'format'         => 'raw',
                'value'          => function($data){

                    return Html::a('Uprawienia',
                        ['/admin/access', 'admin_id' => $data->id],
                        ['class' => 'my-modal-lg']);
                },
                'label'          => 'Uprawienia',
            ],
            [
                'attribute'      => 'email',
                'format'         => 'email',
                'contentOptions' => ['style' => 'width: 300px; max-width: 300px;'],
                'label'          => 'email',
            ],

            [
                'attribute'      => 'role_id',
                'contentOptions' => ['style' => 'width: 100px; max-width: 100px;'],
                'label'          => 'Poziom dostępu',
                'value'          => function($data){
                    return $data->role->name;
                },
                //S0 підключи сюде з класу адмін візуалізацію - рівні доступу
            ],
            [
                'attribute'      => 'created_at',
                'contentOptions' => ['style' => 'width: 150px; max-width: 150px;'],

                'label' => 'Data rejestracji',
                'value' => function($data){
                    return date('Y-m-d', $data->created_at);
                },
            ],

            [
                'attribute'      => 'status',
                'value'          => function($data){
                    return MyStatus::whatStatus($data->status);
                },
                'contentOptions' => ['style' => 'width: 150px; max-width: 100px;'],
                'filter'         => MyStatus::onOff(),
            ],

            ['class'   => 'yii\grid\ActionColumn',
             //'headerOptions' => ['width' => '30'],
             'buttons' => ['update' => function($url, $model){
                 return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['class' => 'my-modal-md', 'data-pjax' => '0']);
             },
                           'view'   => function($url, $model){
                               return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['class' => 'my-modal-md', 'data-pjax' => '0']);
                           },],
            ],

        ],
    ]); ?>
</div>

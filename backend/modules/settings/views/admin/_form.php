<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\entities\Admin */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="admin-form">

    <?php $form = ActiveForm::begin(['id' => 'client-form', 'enableAjaxValidation' => true]); ?>

    <?= $form->field($model, 'username')
        ->textInput(['maxlength' => true])
        ->label('Imię i nazwisko') ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'password')->textInput(['maxlength' => true])->label('Hasło') ?>
    <?= $form->field($model, 'status')
        ->textInput()
        ->dropDownList(\common\statuses\MyStatus::onOff())

    ?>
    <?php

    echo $form->field($model, 'role_id')
        ->textInput()
        ->dropDownList(\common\entities\Role::getAllRoles())
        ->label('Poziom dostępu')
    ?>

    <div class="form-group" style="text-align: right">
        <?= Html::submitButton('Zapisz', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


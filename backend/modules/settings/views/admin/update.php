<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model core\entities\Admin */

$this->title = Yii::t('app', 'Edytuj: {nameAttribute2}', [
    'nameAttribute2' => $model->username,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Admins'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="admin-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

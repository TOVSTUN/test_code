<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model core\entities\Admin */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Admins'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admin-view">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= DetailView::widget([
        'model'      => $model,
        'attributes' => [
            'username:raw:Imię i nazwisko',
            'email:email',
            [
                'attribute' => 'created_at',
                'label'     => 'Dodano',
                'format'    => 'raw',
                'value'     => function($data){
                    return date('Y-m-d', $data->created_at);
                },
            ],
            [
                'attribute' => 'role_id',
                'label'     => 'Poziom dostępu',
                'format'    => 'raw',
                'value'     => function($data){
                    return $data->role->name;
                },
            ],

        ],
    ]) ?>


    <div style="text-align: right">
        <?= Html::a(Yii::t('app', 'Zablokuj'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data'  => [
                'confirm' => Yii::t('app', 'Czy naprawdę chcesz zablokować tego użytkownika?'),
                'method'  => 'post',
            ],
        ]) ?>
        <?= Html::a(Yii::t('app', 'Edytój'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary my-modal-mg']) ?>

    </div>
</div>

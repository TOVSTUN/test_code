<?php

/**
 * Created by PhpStorm.
 * User: tovstun
 * Date: 06.06.19
 * Time: 12:17
 */

use common\components\HtmlHelper;

/* @var $this \yii\web\View */
/* @var $companies common\entities\Company */
/* @var $company common\entities\Company */
/* @var $category common\entities\Category */

/* @var $admin common\entities\Admin */

$this->title = '<strong>' . $admin->username . '</strong> - dostęp do systemy';

?>

    <div class="config-access-form " style="font-size: 18px;">

        <h1><?= $this->title ?></h1>

        <div class="row" style="margin: 50px">
            <div class="col-sm-6">
                <?php
                if(is_array($categories)){

                    foreach($categories as $category_id => $category_name){

                        $icon = HtmlHelper::getOffIcon();
                        if(in_array($category_id, $category_access)){
                            $icon = HtmlHelper::getOnIcon();
                        }
                        echo '<div style="height: 35px"><a class="toggle-category" data-category_id="' . $category_id . '">' . $icon . '<span class="cona">' . $category_name . '</span></a></div>';

                    }
                }
                ?>
            </div>
            <div class="col-sm-6">
                <?php
                if(is_array($types)){
                    foreach($types as $type_id => $type_name){

                        $icon = HtmlHelper::getOffIcon();

                        if(in_array($type_id, $types_access)){
                            $icon = HtmlHelper::getOnIcon();
                        }
                        echo '<div style="height: 35px"><a class="toggle-type" data-type_id="' . $type_id . '">' . $icon . '<span class="cona">' . $type_name . '</span></a></div>';

                    }
                }
                ?>
            </div>

        </div>

    </div>


<?php
$script = <<< JS
    $('.toggle-category').click(function () {            
           var ii = $(this);
        $.ajax({
            type: "POST",
            url: '/admin/access',
            data: {
                category_id   : $(this).data('category_id'),
                admin_id: {$admin->id},    
            },
            success: function(info){
									//alert(ii.find('.toggle-icon').text());
									ii.find('.toggle-icon').replaceWith(info);
									
							  }           
        });
    });

 $('.toggle-type').click(function () {            
           var ii = $(this);
        $.ajax({
            type: "POST",
            url: '/admin/access',
            data: {
                type_id   : $(this).data('type_id'),
                admin_id: {$admin->id},    
            },
            success: function(info){
									//alert(ii.find('.toggle-icon').text());
									ii.find('.toggle-icon').replaceWith(info);
									
							  }           
        });
    });

JS;
$this->registerJs($script);
?>
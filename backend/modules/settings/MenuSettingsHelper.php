<?php
/**
 * Created by PhpStorm.
 * User: tovstun
 * Date: 31.03.19
 * Time: 12:27
 */

namespace backend\modules\settings;


use common\base\TopMenuAbstract;
use Yii;

class MenuSettingsHelper extends TopMenuAbstract {
    public static function showBreadctumbs($obj, $company = null){
        $obj->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Мови'), 'url' => ['/settings/language']];
        $obj->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Країни'), 'url' => ['/settings/country']];
        $obj->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Меню'), 'url' => ['/settings/menu']];
        $obj->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Пункти меню'), 'url' => ['/settings/menu-item']];
        //   $obj->params['breadcrumbs'][] = $obj->title;
    }
}
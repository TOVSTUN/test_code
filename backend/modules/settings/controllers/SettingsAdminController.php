<?php

namespace backend\modules\settings\controllers;

use core\entities\Admin;
use core\entities\User;
use core\forms\admin\EditPasswordForm;
use core\helpers\CropImgHelper;
use frontend\config\ControllerForUser;
use Yii;
use yii\web\Controller;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\web\Response;

class SettingsAdminController extends Controller {
    public function actionPassword(){
        $model = new EditPasswordForm();

        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if($model->load(Yii::$app->request->post()) && $model->savePassword()){

            Yii::$app->session->setFlash('info', Yii::t('app', 'Ваш пароль успішно змінено'));
            return $this->redirect(Yii::$app->request->referrer);

        }

        return $this->render('password', compact('model'));
    }

    public function actionAvatar(){
        //s0 винести цей екшен все в  поведінку і підключати одночасно на фронтенді і бекенді через behevior
        Yii::$app->controller->enableCsrfValidation = false;
        $crop = new CropImgHelper(
            isset($_POST['avatar_src']) ? $_POST['avatar_src'] : null,
            isset($_POST['avatar_data']) ? $_POST['avatar_data'] : null,
            isset($_FILES['avatar_file']) ? $_FILES['avatar_file'] : null
        );
        $src = Yii::getAlias('@webroot') . $crop->getResult();
        $user = User::findOne(Yii::$app->user->id);
        $user->scenario = User::SCENARIO_AVATAR;
        $user->avatar = base64_encode(file_get_contents($src));
        $user->save();
        unlink($src);
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionProfile(){
        $model = Admin::findOne(\Yii::$app->user->identity->getId());
        //  $model->scenario = User::SCENARIO_NEW_USER;


        if($model->load(Yii::$app->request->post())){

            if($model->getDirtyAttributes(['email'])){
                //S0 потрібно відправити активаційни емайл для зміни email
                Yii::$app->session->setFlash('info', Yii::t('app', 'Ваш email успішно змінено, ми вислали вам листа, зайдіть на пошту та активуйте її, інакше ви можете втратити свій аккаунт'));

            }
            if($model->getDirtyAttributes(['username'])){
                Yii::$app->session->setFlash('warning', Yii::t('app', 'Ваше імя успішно змінено'));

            }

            $model->save();
            return $this->redirect(Yii::$app->request->referrer);
        }

        return $this->render('profile', compact('model'));
    }

    public function actionDeleteAccount(){
        return $this->render('delete-account');
    }

}

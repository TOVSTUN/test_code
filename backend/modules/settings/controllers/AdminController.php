<?php

namespace backend\modules\settings\controllers;

use backend\models\AddNewAdmin;
use common\entities\Admin;
use common\entities\AdminHasCategory;
use common\entities\AdminHasCompany;
use common\entities\AdminHasType;
use common\entities\Category;
use common\entities\Company;
use common\entities\Role;
use common\entities\search\AdminSearch;
use common\entities\Type;
use common\statuses\MyStatus;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * //ДОРОБИТИ цей конторолер зараз працює не тільки з адмінаістаторами системи ай модераторами та тлумачами, потрібно розділяти тлумічів та адмінстраторів або я придумати якусь адекватну поліитку повелінки цих обєкиів
 * AdminController implements the CRUD actions for Admin model.
 */
class AdminController extends Controller {
    /**
     * @inheritdoc
     */
    public function behaviors(){
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionAccess($admin_id = null){

        if(Yii::$app->request->isAjax && $admin_id = Yii::$app->request->post('admin_id')){

            $admin_id = Yii::$app->request->post('admin_id');
            $admin = $this->findModel($admin_id);
            if($category_id = Yii::$app->request->post('category_id')){
                return $admin->switchAccessCategory($category_id);
            }
            if($type_id = Yii::$app->request->post('type_id')){
                return $admin->switchAccessType($type_id);
            }

        }

        $admin_id = $admin_id > 0 ? $admin_id : Yii::$app->request->get('admin_id');
        $admin = $this->findModel($admin_id);

        $categories = Category::find()->all();
        $category_access = AdminHasCategory::find()->where(['admin_id' => $admin->id])->all();
        $category_access = ArrayHelper::getColumn($category_access, 'category_id');

        $types = Type::find()->all();
        $types_access = AdminHasType::find()->select('type_id')->where(['admin_id' => $admin->id])->asArray()->all();
        $types_access = ArrayHelper::getColumn($types_access, 'type_id');


        $categories = ArrayHelper::map($categories, 'id', 'name');
        $types = ArrayHelper::map($types, 'id', 'name');


        return $this->renderAjax('access', compact('admin', 'categories', 'category_access', 'types', 'types_access'));
    }

    public function actionIndex2($admin_id = null){
        $admin_id = $admin_id > 1 ? $admin_id : Yii::$app->request->post('admin_id');

        $admin = $this->findModel($admin_id);

        if(Yii::$app->request->isAjax && $company_id = Yii::$app->request->post('company_id')){

            return $admin->switchAccessCategory($company_id);

        }

        $categories = Category::find()->all();
        $category_access = AdminHasCategory::find()->where(['admin_id' => $admin->id])->all();
        $roles = Role::find()->all();
        $roles_access = AdminHasType::find()->where(['admin_id' => $admin->id])->all();


        return $this->renderAjax('access', compact('admin', 'categories', 'category_access', 'roles', 'roles_access'));
    }

    public function actionIndex(){
        $searchModel = new AdminSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Admin model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id){
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Admin model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate(){
        $model = new AddNewAdmin();

        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if($model->load(Yii::$app->request->post())){

            if($user = $model->createAdmin()){
                $this->redirect(Yii::$app->request->referrer);
            }
        }
        return $this->renderAjax('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Admin model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id){

        $model = $this->findModel($id);

        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if($model->load(Yii::$app->request->post()) && $model->save()){
            return $this->redirect(Yii::$app->request->referrer);
        }

        return $this->renderAjax('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Admin model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id){
        $admin = $this->findModel($id);
        $admin->status = MyStatus::STATUS_INACTIVE;
        $admin->save();

        Yii::$app->session->setFlash("Użytkownik " . $admin->username . "został zablokowany");


        return $this->redirect(['index']);
    }

    /**
     * Finds the Admin model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Admin the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id){
        if(($model = Admin::findOne($id)) !== null){
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}

<?php

namespace backend\modules\settings\controllers;

use common\entities\Menu;
use common\entities\MenuItem;
use common\entities\search\MenuSearch;
use common\helpers\StringHelper;
use common\statuses\MyStatus;
use Yii;
use yii\db\StaleObjectException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MenuController implements the CRUD actions for Menu model.
 */
class MenuController extends Controller {
    /**
     * @inheritdoc
     */
    public function behaviors(){
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Menu models.
     * @return mixed
     */
    public function actionIndex(){
        $searchModel = new MenuSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Menu model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate(){
        $model = new Menu();

        if($model->load(Yii::$app->request->post()) && $model->save()){
            return $this->redirect(Yii::$app->request->referrer);
        }

        return $this->renderAjax('create', [
            'model' => $model,
        ]);
    }

    /**Кнтнструкто меню, тут ми конструюємо методом перетягування меню
     * @param $id
     * @return int|string
     * @throws NotFoundHttpException
     */
    public function actionConfig($id){
        $menu = $this->findModel($id);

        if(Yii::$app->request->post()){
            return $this->services($menu);
        }
        else{
            //  print_r($global_menu_items); exit();
            return $this->render('constructor-menu', compact('menu'));
        }
    }

    private function services(Menu $menu){
        if(Yii::$app->request->post('string_serialize')){
            $menu->items_string = Yii::$app->request->post('string_serialize');
            $menu->save();
            return 1;
        }
        if(Yii::$app->request->post('command') == 'arhive'){
            $menu->old_items_string = $menu->items_string;
            $menu->save();
            return 1;
        }
        if(Yii::$app->request->post('command') == 'restore'){
            $menu->items_string = $menu->old_items_string;
            $menu->save();
            return 'reload';
        }
    }

    /**
     * Displays a single Menu model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id){
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Updates an existing Menu model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id){
        $model = $this->findModel($id);

        if($model->load(Yii::$app->request->post()) && $model->save()){
            return $this->redirect(Yii::$app->request->referrer);
        }

        return $this->renderAjax('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id){
        $menu = $this->findModel($id);
        if($menu->delete()){
            MenuItem::deleteAll(['id' => StringHelper::convertToSimpleArray($menu->items_string)]);
        }

        return $this->redirect(['index']);

    }

    protected function findModel($id){
        if(($model = Menu::findOne($id)) !== null){
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}

<?php

namespace backend\modules\settings\models;

use common\entities\Menu;
use common\entities\MenuItem;
use common\statuses\MyStatus;
use yii\base\Model;
use yii\bootstrap\Html;

/**
 * Created by PhpStorm.
 * User: tovstun
 * Date: 02.04.19
 * Time: 9:26
 */
class ConstructorMenu extends Model {

    public $items;
    public $list_id = [];
    public $urls;

    const U_EDIT = '/settings/menu-item/update?id=';
    const U_VIEW = '/settings/menu-item/view?id='; // стежка на екшен view цього контролета

    public function init(){
        parent::init();

        $this->items = MenuItem::find()->indexBy('id')->all();
    }

    public function getGlobalItemList(){
        $items = $this->items;

        echo ' <div class="dd" id="nestable2">';

        echo '  <ol class="dd-list">';
        foreach($items as $item){
            if($item->global == 0) continue;
            ?>

        <li class="dd-item" data-id="<?= $item->id ?>">
            <div class="dd-handle">
                <?= $item->name ?>
                <?= " - " . $item->url ?>
            </div>
            <span style='float: right' class='fdfd'>

            <?= Html::a('<i class="fa fa-cog" aria-hidden="true"></i>',
            [self::U_EDIT . $item->id],
            ['class' => 'my-modal-lg cog-menu'])

            ?>
            <?php
            echo '</span>';
            echo '</li>';

        }

        echo '</ol>';
        echo '</div>';
    }

    public function getConstructorMenu(Menu $menu){

        if(strlen($menu->items_string) > 5){
            $this->listGenerate(json_decode($menu->items_string, true), $this->list_id);
        }
        else{
            echo '<div class="dd-empty"></div>';
        }
    }

    private function listGenerate($decode, &$list_id){
        echo '<ol class="dd-list">';
        foreach($decode as $item){

            if(isset($this->items[$item['id']])){

                echo "<li class='dd-item' data-id='{$item['id']}'>" . $this->itemLI($this->items[$item['id']]);

                if(isset($item['children'])) echo $this->listGenerate($item['children'], $list_id);
                // unset($menu_items_array[$item['id']]);
                $list_id[] = $item['id'];
                echo '</li>';
            }
        }
        echo '</ol>';
    }

    private function itemLI($list_item){

        $off = null;
        if($list_item->status != MyStatus::STATUS_ACTIVE) $off = 'style="background: #ff00001a;"';
        if($list_item->url) $list_item->url = ' - <span class = "gray50">' . $list_item->url . '</span>';
        return "<div class='dd-handle' $off>
                            $list_item[name]   $list_item[url]
                            <div style='float: left'></div>
                            <div style='float: right'>
                                <i class='nestable-menu-icom fa $list_item->icon' aria-hidden='true'></i>
                            </div>
                        </div>
                        <span style='float: right' class='fdfd'>
                        <!--<div class=\"switch\">
                                    <div class=\"onoffswitch\">
                                        <input type=\"checkbox\" checked class=\"onoffswitch-checkbox\" id=\"example1\">
                                        <label class=\"onoffswitch-label\" for=\"example1\">
                                            <span class=\"onoffswitch-inner\"></span>
                                            <span class=\"onoffswitch-switch\"></span>
                                        </label>
                                    </div>
                                </div>-->


                            "

            . Html::a('<i class="fa fa-cog" aria-hidden="true"></i>',
                [self::U_EDIT . $list_item['id']],
                ['class' => 'my-modal-lg cog-menu'])
            .

            "</span>";
    }
}
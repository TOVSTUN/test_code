<?php

use common\components\MyAccessRule;
use common\entities\Admin;

$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id'                  => 'app-backend',
    'basePath'            => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'layout'              => '@backend/layouts/inspira/main-template',
    'defaultRoute'        => 'document/index',
    'bootstrap'           => ['log'],
    'modules'             => [
        'mail'     => [
            'class'     => 'backend\modules\mail\MailModule',
            'as access' => MyAccessRule::globalRule([Admin::IS_ROOT, Admin::IS_MANAGER]),
        ],
        'settings' => [
            'class'     => 'backend\modules\settings\SettingsModule',
            'as access' => MyAccessRule::globalRule([Admin::IS_ROOT, Admin::IS_ACCOUNTANT]),
        ],
    ],

    'components' => [
        'helper' => [
            'class' => 'backend\components\Helper',
        ],

        'request'      => [
            'csrfParam' => '_csrf-backend',
        ],
        'user'         => [
            'identityClass'   => 'common\entities\Admin',
            'enableAutoLogin' => true,
            'identityCookie'  => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'session'      => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log'          => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'  => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName'  => false,
            'rules'           => [
                'admin/access'                                            => 'settings/admin/access',
                'admin/<action:(index|index2|view|create|delete|access)>' => 'settings/admin/<action>',
                'document/remove-file/<document_id:(\d+)>/<hash:>'        => 'document/remove-file',
                //реєстрація авторизація
                'signup'                                                  => 'site/signup',
                'login'                                                   => 'site/login',
                'logout'                                                  => 'site/logout',
            ],
        ],

    ],
    'aliases'    => [
        //аліаси шаблонів
        '@lite-template' => '@backend/layouts/inspira/lite-template',
        '@main-template' => '@backend/layouts/inspira/main-template',
    ],
    'params'     => $params,
];

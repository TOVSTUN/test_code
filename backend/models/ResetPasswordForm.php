<?php
/**
 * Created by PhpStorm.
 * User: tovstun
 * Date: 20.09.18
 * Time: 13:41
 */

namespace backend\models;


use common\entities\Admin;
use Yii;
use yii\base\Model;

class ResetPasswordForm extends Model {
    public $oldPassword;
    public $newPassword;
    public $newPassword2;


    public function rules(){
        return [
            // username and password are both required
            [['newPassword', 'newPassword2'], 'required'],
            [['newPassword', 'newPassword2'], 'string', 'min' => 7],
            [['newPassword2'], 'comparePasswords'],
        ];
    }


    public function validatePassword($attribute, $params){
        if(!$this->hasErrors()){
            $user = Admin::findIdentity(\Yii::$app->user->id);
            if(!$user || !$user->validatePassword($this->oldPassword)){
                $this->addError($attribute, \Yii::t('app', 'Incorrect password.'));
            }
        }
    }

    public function comparePasswords($attribute, $params){
        if($this->newPassword !== $this->newPassword2)
            $this->addError($attribute, \Yii::t('app', 'Пароли несовпадают'));
    }

    public function savePassword(){
        if($this->validate()){
            $user = Admin::findIdentity(Yii::$app->user->id);
            $user->setPassword($this->newPassword);
            if($user->save()) return true;
        }
        return null;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: tovstun
 * Date: 20.12.18
 * Time: 10:03
 */

namespace backend\models;

use common\entities\Admin;
use yii\base\Model;

class AddNewAdmin extends Model {

    public $username;
    public $email;
    public $password;
    public $status;
    public $role_id;
    public $avatar_default = '';

    /**
     * @inheritdoc
     */
    public function rules(){
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => 'common\entities\Admin', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 6, 'max' => 45],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 45],
            ['email', 'unique', 'targetClass' => 'common\entities\Admin', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 8],
            [['role_id', 'status'], 'required'],
            [['role_id', 'status'], 'integer'],
        ];
    }

    /**
     * Signs user up.
     *
     * @return Admin|null the saved model or null if saving fails
     */
    public function createAdmin(){
        if(!$this->validate()){
            return null;
        }
        $admin = new Admin();
        $admin->username = $this->username;
        $admin->email = $this->email;
        $admin->setPassword($this->password);
        $admin->generateAuthKey();
        $admin->status = $this->status;
        $admin->role_id = $this->role_id;
        $admin->avatar = $this->avatar_default;

        return $admin->save() ? $admin : null;
    }
    /*  public function updateAdmin($id){

          if (!$this->validate()) {
              return null;
          }
          $admin = Admin::findOne($id);

          $admin->username = $this->username;
          $admin->email = $this->email;
          $admin->setPassword($this->password);
          $admin->status = $this->status;
          $admin->role_id = $this->role_id;

          return $admin->save() ? $admin : null;
      }*/

}
<?php
/**
 * Created by PhpStorm.
 * User: tovstun
 * Date: 20.01.17
 * Time: 22:09
 */

namespace backend\models;

use common\entities\Admin;
use yii\base\Model;

class AdminSignupForm extends Model {
    public $username;
    public $email;
    public $password;
    public $avatar_default = '';


    /**
     * @inheritdoc
     */
    public function rules(){
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => 'common\entities\Admin', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 6, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => 'common\entities\Admin', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 7],
        ];
    }

    /**
     * Signs user up.
     *
     * @return Admin|null the saved model or null if saving fails
     */
    public function signup(){
        if(!$this->validate()){
            return null;
        }

        $admin = new Admin();
        $admin->username = $this->username;
        $admin->email = $this->email;
        $admin->setPassword($this->password);
        $admin->generateAuthKey();
        $admin->status = 10;
        $admin->avatar = '123';
        $admin->role_id = 3;

        return $admin->save() ? $admin : null;
    }
}
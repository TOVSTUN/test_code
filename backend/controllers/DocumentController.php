<?php

namespace backend\controllers;

use common\entities\Admin;
use common\entities\File;
use common\entities\Helper;
use common\statuses\MyStatus;
use Worker;
use Yii;
use common\entities\Document;
use common\entities\search\DocumentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\widgets\ActiveForm;

/**
 * DocumentController implements the CRUD actions for Document model.
 */
class DocumentController extends Controller {
    public function beforeAction($action){

        $this->enableCsrfValidation = false;

        if(in_array($action->id, ['index'])){
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    public function behaviors(){
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Document models.
     * @return mixed
     */
    public function actionIndex(){
        $worker_id = Yii::$app->user->identity->getId();
        $worker = Admin::findOne($worker_id);
        $allowedCategories = $worker->getAllowedCategories();
        $allowedTypes = $worker->getAllowedTypes();
        $searchModel = new DocumentSearch($allowedCategories, $allowedTypes);
        if($a = Yii::$app->request->get('DocumentSearch') ?? false){
            $searchModel->company_name = $a['company_name'];
            $searchModel->company_acronim = $a['company_acronim'];
        }
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Document model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id){
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Document model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate(){
        $model = new Document();
        $model->number = Helper::generateNumber();
        $model->created_admin_id = Yii::$app->user->getId();

        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())){
            $model->imageFile = UploadedFile::getInstances($model, 'imageFile');
            if(!empty($model->imageFile)){
                $model->scenario = 'fileUploaded';
            }
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if($model->load(Yii::$app->request->post())){
            $tmp = $model->imageFile = UploadedFile::getInstances($model, 'imageFile');
            if(!empty($model->imageFile)){
                $model->scenario = 'fileUploaded';
            }

            if(!$model->date_end_str){
                $model->date_end_str = '2045-06-10';
                // $model->date_end = 2147483647;
            }
            if($model->save()){
                Helper::confirmNumber();
                $model->imageFile = $tmp;
                if($model->upload()){
                    Yii::$app->session->setFlash('info', "Wszystko ok");
                }
            }


            return $this->redirect(Yii::$app->request->referrer);
        }

        return $this->renderAjax('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Document model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id){
        $model = $this->findModel($id);

        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())){
            $model->imageFile = UploadedFile::getInstances($model, 'imageFile');
            if(!empty($model->imageFile)){
                $model->scenario = 'fileUploaded';
            }
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }


        if($model->load(Yii::$app->request->post())){
            if(!$model->date_end_str){
                $model->date_end_str = '2045-06-10';
                // $model->date_end = 2147483647;
            }
            $tmp = $model->imageFile = UploadedFile::getInstances($model, 'imageFile');
            if(!empty($model->imageFile)){
                $model->scenario = 'fileUploaded';
            }
            $model->save();
            $model->imageFile = $tmp;
            if($model->upload()){
                Yii::$app->session->setFlash('info', "Wszystko ok");
            }
            return $this->redirect(Yii::$app->request->referrer);
        }

        return $this->renderAjax('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Document model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id){

        $user = Admin::findOne(Yii::$app->user->identity->getId());
        if($user->role->id == 1){
            $files = File::find()->where(['document_id' => $id])->all();
            try {
                foreach($files as $file){
                    unlink('uploads/' . $file->hash);
                    $file->delete();
                }
            } catch(\Exception $e){
                Yii::$app->session->setFlash("danger", "Brak możliwości usunięcia pliku, zgloś to administratorowi");
            }
            $this->findModel($id)->delete();
            Yii::$app->session->setFlash('info', 'Dokument usunięto');
        }
        else  Yii::$app->session->setFlash("danger", "Brak uprawień");

        return $this->redirect(['index']);
    }

    public function actionRemoveFile($document_id, $hash){
        if(Yii::$app->user->identity){
            $file = File::find()->where(['hash' => $hash])->one();
            if($file && $file->document_id == $document_id){

                $file->delete();
                unlink('uploads/' . $hash);
                Yii::$app->session->setFlash('info', "Plik $file->name z dokumentu skutećno wydaleno");

            }
        }
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionClose($document_id){
        $document = $this->findModel($document_id);
        $document->status = MyStatus::STATUS_INACTIVE;
        $document->save();
        Yii::$app->session->setFlash("info", "Umowa " . $document->number . 'zakończona i  wyslana do archiwum');
        return $this->redirect(['index']);
    }

    public function actionActivate($document_id){
        $document = $this->findModel($document_id);
        $document->status = MyStatus::STATUS_ACTIVE;
        $document->save();
        Yii::$app->session->setFlash("info", "Umowa " . $document->number . 'aktywowana');
        return $this->redirect(['index']);
    }

    /**
     * Finds the Document model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Document the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id){
        if(($model = Document::findOne($id)) !== null){
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

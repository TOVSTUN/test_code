<?php

namespace backend\controllers;

use backend\models\AdminLoginForm;
use backend\models\AdminSignupForm;
use backend\models\ResetPasswordForm;
use common\entities\Admin;
use common\entities\Archive;
use common\entities\Company;
use common\entities\ProductGroup;
use common\entities\server_mor\CenySpinka;
use common\entities\server_mor\KonfiguracjaMor;
use common\entities\server_mor\ZasobySpinka;
use common\entities\TmpLocker;
use common\entities\Worker;
use common\helpers\MyExcelReader;
use common\services\parsers\HistoryInfoParserExcel;
use common\services\Synchronizer;
use common\services\SyncService;
use common\entities\server_mor\CompanySpinka;
use common\entities\server_mor\DocNagSpinka;
use common\entities\server_mor\DocElemSpinka;
use common\entities\server_mor\ProductItemSpinka;
use common\entities\server_mor\ProductGroupSpinka;
use common\entities\server_mor\ProductPriceSpinka;
use common\entities\server_mor\WarehouseSpinka;
use common\services\SyncService2;
use common\services\WorkierService;
use common\slices\WarehouseSlice;
use common\slices\WarehouseSpinkaSlice;
use common\statuses\MyStatus;
use Yii;
use yii\db\mssql\PDO;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\User;

/**
 * Site controller
 */
class SiteController extends Controller {
    /**
     * {@inheritdoc}
     */
    public function behaviors(){
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'signup'],
                        'allow'   => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'error', 'reset-password'],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions(){
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionSynchronization(){


    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        /** Цей блок для парсинуу історії видань
         * $file = Yii::getAlias('@webroot') . '/uploads/old_jabil/3.xls';
         * $info = HistoryInfoParserExcel::run2($file, 24864);
         * print_r($info); exit;
         */
//print_r('index ok'); exit;
        //  Archive::deleteAll(['company_id'=>33484]);
        //  exit();

        //   $file = Yii::getAlias('@webroot') . '/uploads/old_vi/1.xlsx';
        //  $info = HistoryInfoParserExcel::run2($file, 33484);
        //   print_r($info); exit;

        //$file = Yii::getAlias('@webroot') . '/uploads/szafki_1.xls';
        //$info = HistoryInfoParserExcel::runLocker($file, 32525);
        //print_r($info); exit;


        /*for($i = 0; $i <= 50; $i++){
            Archive::parse();
        }*/

        // TmpLocker::parse();
    }


    public function actionResetPassword(){

        $model = new ResetPasswordForm();

        if($model->load(Yii::$app->request->post()) && $model->savePassword()){
            Yii::$app->session->setFlash('info', 'Twoje hasło zostało zmienione');
            return $this->redirect(Yii::$app->request->referrer);
        }
        else{
            return $this->renderAjax('reset-password', [
                'model' => $model,
            ]);
        }

        // $user = Admin::findOne(Yii::$app->user->getId());
        // print_r($user); exit;
    }

    public function actionLogin(){
        $this->layout = '@lite-template';
        if(!Yii::$app->user->isGuest){
            return $this->redirect('/');
        }

        $model = new AdminLoginForm();
        if($model->load(Yii::$app->request->post()) && $model->login()){
            return $this->redirect('/document/index');
        }
        else{
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }
    /* public function actionSignup()
     {
         $this->layout = '@lite-template';
         $model = new AdminSignupForm();
         if ($model->load(Yii::$app->request->post())) {
             if ($user = $model->signup()) {
                 if (Yii::$app->getUser()->login($user)) {
                     return $this->goHome();
                 }
             }
         }

         return $this->render('signup', [
             'model' => $model,
         ]);
     }*/
    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout(){
        Yii::$app->user->logout();

        return $this->goHome();
    }
}

<?php

/* @var $this \yii\web\View */

/* @var $content string */

use backend\layouts\inspira\assets\InspiraAsset;
use yii\helpers\Html;


InspiraAsset::register($this);
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="w idth=device-width, initial-scale=1.0">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>


    <body>
    <?php $this->beginBody() ?>

    <div class="gray-bg" style="height:100%">
        <?= $content ?>
    </div>

    <?php $this->endBody() ?>

    </body>
    </html>
<?php $this->endPage() ?>
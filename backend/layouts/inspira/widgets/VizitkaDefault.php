<?php
/**
 * Created by PhpStorm.
 * User: tovstun
 * Date: 19.05.18
 * Time: 10:55
 */

namespace backend\layouts\inspira\widgets;


use Yii;
use yii\base\Widget;
use yii\helpers\Html;

class VizitkaDefault extends Widget {
    public function init(){
        parent::init();
    }


    public function run(){

        //    $this->view->registerCssFile('/assets-avatar/css/main.css');
        //  $this->view->registerCssFile('/assets-avatar/dist/cropper.min.css');
        // $this->view->registerJsFile('/assets-avatar/js/main.js');
        // $this->view->registerJsFile('/assets-avatar/dist/cropper.min.js');

        $userName = \Yii::$app->user->identity->username;
        //$statusType = MyStatusUser::getUserRole(\Yii::$app->user->identity->user_role_id);
        ?>
        <ul class="nav metismenu">
            <li class="nav-header">
                <div class="dropdown profile-element" align="center">

                    <div class="" id="crop-avatar">

                        <div class="avatar-view">
                            <a>
                                <img alt="image"
                                     class="img-circle img-lg"
                                     src="data:image/jpg;base64,<?= Yii::$app->user->identity->avatar ?>"
                                     style="cursor: pointer">
                            </a>
                        </div>


                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs">
                                <strong class="font-bold\">
                                    <?= $userName ?>
                                </strong>
                             </span>
                                <span class="text-muted text-xs block">
                Admin
                                    <b class="caret"></b>
                                </span>
                            </span> </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li><a href="/settings/profile">
                                    <?= \Yii::t('app', 'Profile') ?>
                                </a></li>
                            <li><a href="/settings/password">
                                    <?= 'Zmiana hasła' ?>
                                </a></li>
                            <li class="divider"></li>
                            <li>
                                <?= Html::beginForm(['/site/logout'], 'post')
                                . Html::submitButton(
                                    \Yii::t('app', 'Logout'),
                                    ['class' => 'btn btn-link logout']
                                )
                                . Html::endForm()
                                ?>
                            </li>
                        </ul>
                    </div>
                    <div class="logo-element">
                        IN+
                    </div>


            </li>
        </ul>

        <?php
    }
}

//\Yii::t('app', 'This is a string to translate!')
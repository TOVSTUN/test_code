<?php
/**
 * Created by PhpStorm.
 * User: tovstun
 * Date: 19.05.18
 * Time: 10:55
 */

namespace backend\layouts\inspira\widgets;


use Yii;
use yii\base\Widget;
use yii\helpers\Html;

class VizitkaForMor extends Widget {
    public function init(){
        parent::init();
    }


    public function run(){

        //    $this->view->registerCssFile('/assets-avatar/css/main.css');
        //  $this->view->registerCssFile('/assets-avatar/dist/cropper.min.css');
        // $this->view->registerJsFile('/assets-avatar/js/main.js');
        // $this->view->registerJsFile('/assets-avatar/dist/cropper.min.js');

        try {
            $userName = \Yii::$app->user->identity->username;
            $role_name = \Yii::$app->user->identity->role_name;
        } catch(\Exception $e){
            Yii::$app->getResponse()->redirect('/login')->send();
            Yii::$app->end();
        }
        //$statusType = MyStatusUser::getUserRole(\Yii::$app->user->identity->user_role_id);
        ?>
        <ul class="nav metismenu">
            <li class="nav-header">
                <div class="dropdown profile-element" align="center">

                    <div class="logo-mor" id="logo-mor">

                    </div>
                    <div>

                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs">
                                <strong class="font-bold\">
                                    <?= $userName ?>
                                </strong>
                             </span>
                                <span class="text-muted text-xs block">
                <?= $role_name ?>
                                    <b class="caret"></b>
                                </span>
                            </span> </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <!--<li><a href="/settings/profile">
                                    <?/*= \Yii::t('app', 'Profile') */ ?>
                                </a></li>-->
                            <li><a href="/site/reset-password" class="my-modal-md">
                                    <?= 'Zmiana hasła' ?>
                                </a></li>
                            <li class="divider"></li>
                            <li>
                                <?= Html::beginForm(['/site/logout'], 'post')
                                . Html::submitButton(
                                    \Yii::t('app', 'Wyloguj'),
                                    ['class' => 'btn btn-link logout']
                                )
                                . Html::endForm()
                                ?>
                            </li>
                        </ul>

                    </div>
                    <div class="logo-element">
                        IN+
                    </div>

            </li>
        </ul>

        <?php
    }
}

//\Yii::t('app', 'This is a string to translate!')
<?php
/**
 * Created by PhpStorm.
 * User: tovstun
 * Date: 24.04.18
 * Time: 0:11
 */

namespace backend\layouts\inspira\widgets;

use yii\base\Widget;
use yii\helpers\Html;

class NavbarTopAdmin extends Widget {
    public $taskAktive;

    public function init(){
        parent::init();
        $this->taskAktive = '1234';
    }


    public function run(){
        ?>
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i>
                    </a>
                    <!--<form role="search" class="navbar-form-custom" action="search_results.html">
                        <div class="form-group">
                            <input type="text" placeholder="Search for something..." class="form-control"
                                   name="top-search" id="top-search">
                        </div>
                    </form>-->
                </div>

                <ul class="nav navbar-top-links navbar-right">
                    <li>
                        <span class="m-r-sm text-muted welcome-message">

<!--                            Welcome to INSPINIA+ Admin Theme.
-->
                        </span>
                    </li>
                    <!-- <li>
                        <a class="count-info" href="#">
                            <i class="fa fa-envelope"></i> <span class="label label-warning">16</span>
                        </a>
                    </li>
                    <li>
                        <a class="count-info" href="/admin-task">
                            <i class="fa fa-exclamation-circle"></i> <span
                                    class="label label-primary"><?/*= $this->taskAktive */
                    ?></span>
                        </a>

                    </li>-->

                    <!-- -->


                    <?= '<li>'
                    . Html::beginForm(['/site/logout'], 'post')
                    . Html::submitButton(
                        \Yii::t('app', 'Logout'),
                        ['class' => 'btn btn-link logout']
                    )
                    . Html::endForm()
                    . '</li>'; ?>
                </ul>

            </nav>
        </div>

        <?php
    }
}
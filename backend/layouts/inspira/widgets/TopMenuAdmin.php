<?php
/**
 * Created by PhpStorm.
 * User: tovstun
 * Date: 14.09.18
 * Time: 14:41
 */

namespace backend\layouts\inspira\widgets;


class TopMenuAdmin {

    public static function generate(){


        $menu = <<<EOD
<li>
                        <span class="m-r-sm text-muted welcome-message">

        Welcome to .....

                        </span>
                    </li>
                   <li>
                        <a class="count-info" href="#">
                            <i class="fa fa-envelope"></i> <span class="label label-warning">16</span>
                        </a>
                    </li>
                    <li>
                        <a class="count-info" href="/admin-task">
                            <i class="fa fa-exclamation-circle"></i> <span
                                    class="label label-primary"></span>
                        </a>

                    </li>
EOD;
        return $menu;

    }
}
<?php

namespace backend\layouts\inspira\widgets;

use backend\layouts\inspira\components\LeftMenuGenerator;
use common\entities\Admin;
use common\entities\Company;
use common\entities\Menu;

use common\statuses\MyStatus;
use yii\base\Widget;

class LeftMenuInspira extends Widget {
    private $menu;
    private $companies;
    /** @var Admin $admin */
    private $admin;

    public function init(){
        parent::init();


        $this->admin = \Yii::$app->user->identity;
        $this->menu = Menu::findOne([
            'system_name' => 'left_menu',
            'role_id'     => $this->admin->role_id,
            'status'      => MyStatus::STATUS_ACTIVE,
        ]);
        // тут налаштовуємло жоступи до компаній
        // $this->companies =  Company::find()->indexBy('id')->where(['id' =>$this->admin->getAllowedCompanies()])->all();
        $this->companies = Company::find()->indexBy('id')->all();
        // print_r( $this->companies); exit;
    }

    public function run(){

        $js = <<<JS
        //живий пошук по компнніям в меню
 $("#menuSearch").keyup(function(){
     $('.menu-company').removeClass('hide');
	 var search = $("#menuSearch").val();
	 if(search.length>2){	 		 
		$('.nazwa:not(:myContains2('+ search +'))').closest('.menu-company').addClass('hide');		
	 }
});

 //псевдометод який потрібен для селекта 
     
     jQuery.expr[":"].myContains2 = jQuery.expr.createPseudo(function(arg) { 
         return function( elem ) { 
             return jQuery(elem).text().toLowerCase().indexOf(arg.toLowerCase()) >= 0; 
         }; });      

JS;


        $this->getView()->registerJs($js);
        //   if (!$this->menu) return ''; // як що меню пусте то виходимо з  меню генератра
        // тут іідключаємо меню для потрібної групи користувачів
        return (new LeftMenuGenerator($this->admin, $this->companies))->zgenerujMenu($this->menu);
    }

}

?>
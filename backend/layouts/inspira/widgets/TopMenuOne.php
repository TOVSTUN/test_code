<?php
/**
 * Created by PhpStorm.
 * User: tovstun
 * Date: 05.04.19
 * Time: 12:02
 */

namespace backend\layouts\inspira\widgets;


use common\base\TopMenuAbstract;
use common\entities\Company;

/**
 * Цей клас генерує верхне меню по типу хлібних крихт
 */
class TopMenuOne extends TopMenuAbstract {
    /**
     * @param $obj
     * @var $company Company
     */
    public static function showBreadctumbs($obj, $company = null){

        $path = [
            //  '/company'                                  => 'Wszystkie firmy',
            //   '/company/' . $company->id . '/info'       => 'O firmie',
            '/absence/' . $company->id                 => 'Nieobecność',
            '/company/' . $company->id . '/department' => 'Dzialy',
            '/company/' . $company->id . '/position'   => 'Stanowiska pracy',
            '/locker/' . $company->id                  => 'Szafki',
            '/company/' . $company->id . '/worker'     => 'Pracownicy',
            '/company/' . $company->id . '/document'   => 'Dokumenty',
            '/received-products/' . $company->id       => 'Raport wydań',
            '/warehouse/' . $company->id               => 'Magazyn',
            // '/needs/' . $company->id                   => 'Zapotrzebowanie',
            '/access/prosave/' . $company->id          => 'Dostęp',
            //   '/company/' . $company->id . '/access2'        => 'Dostęp - od ' . $company->name, Zapotrzebowanie
        ];

        parent::generateBreadctumbs($obj, $path);
    }

    public static function showMenuWashing($obj){

        $path = [

            '/washing/washing-company' => 'Pralni',
            '/washing/washing-doc'     => 'Zamówienia',

        ];

        parent::generateBreadctumbs($obj, $path);
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: tovstun
 * Date: 24.04.18
 * Time: 0:11
 */

namespace backend\layouts\inspira\widgets;

use common\helpers\MainHelper;
use common\helpers\SecondaryHelper;
use Yii;
use yii\base\Widget;
use yii\helpers\Html;

class NavbarTop extends Widget {

    public function run(){
        ?>
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0; z-index: 10">
                <div class="navbar-header">
                    <!-- <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#">
                         <i class="fa fa-bars"></i>
                     </a>-->
                    <form role="search" class="navbar-form-custom" action="/document">
                        <div class="form-group">
                            <input type="text" placeholder="Wyszukaj umowy po numerze " class="form-control"
                                   name="DocumentSearch[number]" id="top-search">
                        </div>
                    </form>
                </div>


                <ul class="nav navbar-top-links navbar-right">

                    <?= '<li>'
                    . Html::beginForm(['/site/logout'], 'post')
                    . Html::submitButton(
                        \Yii::t('app', 'Wyloguj'),
                        ['class' => 'btn btn-link logout']
                    )
                    . Html::endForm()
                    . '</li>'; ?>
                </ul>

            </nav>
        </div>

        <?php
    }
}
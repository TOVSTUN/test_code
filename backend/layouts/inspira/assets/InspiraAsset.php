<?php

namespace backend\layouts\inspira\assets;

use Yii;
use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class InspiraAsset extends AssetBundle {
    public function init(){
        parent::init();

        // переустановка BootstrapAsset чтобі не грузились собственніе стили
        /*\Yii::$app->assetManager->bundles['yii\\bootstrap\\BootstrapAsset'] = [
            'css' => [],
            'js' => []
        ];*/

        if($this->ceUrl() == 'login') $this->fileLogin();
        elseif($this->ceUrl() == 'dashboard') $this->fileDashboard();
        elseif($this->ceUrl() == 'niestable_list') $this->fileNiestableList();
        elseif($this->ceUrl() == 'niestable_list_worker') $this->fileNiestableListWorker();
        elseif($this->ceUrl() == 'show') $this->fileShow();
        elseif($this->ceUrl() == 'dropzone') $this->fileDropzone();
        elseif($this->ceUrl() == 'advances_form') $this->fileAdvancedForm();
        else  $this->fileDefault();

    }

    public $basePath = '@webroot';
    public $baseUrl = '@web/inspira';
    public $css = [
        //'css/bootstrap.min.css',
        'font-awesome/css/font-awesome.css',
        'css/animate.css',
        'css/style.css',
        'css/style2.css',
        'assets-avatar/css/main.css',
        'assets-avatar/dist/cropper.min.css',
        'css/plugins/dropzone/basic.css',
        'css/plugins/dropzone/dropzone.css',
        'css/plugins/chosen/bootstrap-chosen.css',
    ];
    public $js = [];


    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    private $Flot = [
        //<!-- Flot -->
        'js/plugins/flot/jquery.flot.js',
        'js/plugins/flot/jquery.flot.tooltip.min.js',
        'js/plugins/flot/jquery.flot.spline.js',
        'js/plugins/flot/jquery.flot.resize.js',
        'js/plugins/flot/jquery.flot.pie.js',
        'js/plugins/flot/jquery.flot.symbol.js',
        'js/plugins/flot/jquery.flot.time.js',
    ];
    private $Peity = [
        //<!-- Peity -->
        'js/plugins/peity/jquery.peity.min.js',
        'js/demo/peity-demo.js',
    ];
    private $Custom = [
        //<!-- Custom and plugin javascript -->
        'js/inspinia.js',
        'js/my.js',
        'js/plugins/pace/pace.min.js',
        'assets-avatar/js/main.js',
        'assets-avatar/dist/cropper.min.js',

    ];
    private $JQuery_UI = [
        //<!-- jQuery UI -->
        'js/plugins/jquery-ui/jquery-ui.min.js',
    ];
    private $Jvectormap = [
        //<!-- Jvectormap -->
        'js/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js',
        'js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js',
    ];
    private $EayPIE = [
        //<!-- EayPIE -->
        'js/plugins/easypiechart/jquery.easypiechart.js',
    ];
    private $Sparkline = [
        //<!-- Sparkline -->
        'js/plugins/sparkline/jquery.sparkline.min.js',
    ];
    private $Sparkline_demo = [
        //<!-- Sparkline demo data  -->
        'js/demo/sparkline-demo.js',
    ];
    private $Dropzone = [
        'js/plugins/dropzone/dropzone.js',
    ];

    private $advancedForm = [
        'js/inspinia.js',
        'js/plugins/pace/pace.min.js',
        'js/plugins/slimscroll/jquery.slimscroll.min.js',
        'js/plugins/chosen/chosen.jquery.js',
        'js/plugins/jsKnob/jquery.knob.js',
        'js/plugins/jasny/jasny-bootstrap.min.js',
        'js/plugins/datapicker/bootstrap-datepicker.js',
        'js/plugins/nouslider/jquery.nouislider.min.js',
        'js/plugins/switchery/switchery.js',
        'js/plugins/ionRangeSlider/ion.rangeSlider.min.js',
        'js/plugins/iCheck/icheck.min.js',
        'js/plugins/metisMenu/jquery.metisMenu.js',
        'js/plugins/colorpicker/bootstrap-colorpicker.min.js',
        'js/plugins/clockpicker/clockpicker.js',
        'js/plugins/cropper/cropper.min.js',
        'js/plugins/fullcalendar/moment.min.js',
        'js/plugins/daterangepicker/daterangepicker.js',
        'js/plugins/select2/select2.full.min.js',
        'js/plugins/touchspin/jquery.bootstrap-touchspin.min.js',
        'js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js',
        'js/plugins/dualListbox/jquery.bootstrap-duallistbox.js',

    ];

    private $Mainly = [    //!-- Mainly scripts -->

                           //   'js/jquery-3.1.1.min.js',
                           //  'js/bootstrap.min.js',
                           'js/plugins/metisMenu/jquery.metisMenu.js',
                           'js/plugins/slimscroll/jquery.slimscroll.min.js',
    ];
    private $ICheck = [   //<!-- iCheck -->
                          'js/plugins/iCheck/icheck.min.js',
    ];

    protected function fileLogin(){
        $this->js = array_merge($this->Mainly, $this->ICheck);
        $this->css = array_merge(['css/plugins/iCheck/custom.css'], $this->css);
    }

    protected function fileDefault(){
        $this->js = array_merge(
            $this->JQuery_UI, //
            $this->Mainly, //
            $this->Custom, //
            //рещту можна видаляти
            $this->Flot,
            $this->Peity,
            $this->Jvectormap,
            $this->EayPIE,
            $this->Sparkline
        //['/inspira/js/plugins/footable/footable.all.min.js']
        );
    }

    protected function fileAdvancedForm(){
        $this->js = array_merge(
            $this->JQuery_UI, //
            $this->Mainly, //
            $this->Custom, //
            //рещту можна видаляти
            $this->advancedForm
        );
    }

    protected function fileDropzone(){
        $this->js = array_merge(
            $this->JQuery_UI, //
            $this->Mainly, //
            $this->Custom, //
            //рещту можна видаляти
            $this->Flot,
            $this->Peity,
            $this->Jvectormap,
            $this->EayPIE,
            $this->Dropzone
        //['/inspira/js/plugins/footable/footable.all.min.js']
        );
    }

    protected function fileShow(){
        $this->js = array_merge(
            $this->JQuery_UI,
            $this->Mainly,
            $this->Custom,
            ['/inspira/js/plugins/footable/footable.all.min.js']
        );
    }

    protected function fileDashboard(){
        $this->js = array_merge(
            $this->Mainly,
            $this->Flot,
            $this->Peity,
            $this->Custom,
            $this->JQuery_UI,
            $this->Jvectormap,
            $this->EayPIE,
            $this->Sparkline
        );
    }

    private function ceContr(){
        return Yii::$app->controller->id;
    }

    private function ceAct(){
        return Yii::$app->controller->action->id;
    }

    private function ceUrl(){
        return isset(Yii::$app->controller->view->params['url']) ? Yii::$app->controller->view->params['url'] : null;
    }

    private function fileNiestableList(){
        //<!-- Nestable List -->
        $nestable = [
            'js/plugins/nestable/jquery.nestable.js',
            'js/nestable-support.js',
        ];

        $this->js = array_merge(
            $this->Mainly,
            $nestable,
            $this->Custom
        );
    }

    private function fileNiestableListWorker(){
        //<!-- Nestable List -->
        $nestable = [
            'js/plugins/nestable/jquery.nestable-worker.js',
            'js/nestable-support-worker.js',
        ];

        $this->js = array_merge(
            $this->Mainly,
            $nestable,
            $this->Custom
        );
    }
}

<div class="modal fade" id="avatar-modal" aria-hidden="true" aria-labelledby="avatar-modal-label"
     role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form class="avatar-form" action="/cropavatar" enctype="multipart/form-data"
                  method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="avatar-modal-label">Change Avatar</h4>
                </div>
                <div class="modal-body">
                    <div class="avatar-body">

                        <!-- Upload image and data -->
                        <div class="avatar-upload">
                            <input type="hidden" class="avatar-src" name="avatar_src">
                            <input type="hidden" class="avatar-data" name="avatar_data">

                        </div>

                        <!-- Crop and preview -->
                        <div class="row">
                            <div class="col-md-9">
                                <div class="avatar-wrapper">

                                    <img src="" class="cropper-hidden">
                                </div>
                                <input type="file" class="avatar-input" id="avatarInput"
                                       name="avatar_file">
                            </div>
                            <div class="col-md-3">
                                <div class="avatar-preview preview-lg"></div>
                                <div class="avatar-preview preview-md hide"></div>
                                <div class="avatar-preview preview-sm hide"></div>
                            </div>
                        </div>

                        <div class="row avatar-btns">
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-primary btn-block avatar-save">
                                    Обрезать и сохранить
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div> -->
            </form>
        </div>
    </div>
</div>
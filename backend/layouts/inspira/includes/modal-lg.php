<?php

use yii\bootstrap\Modal;

/* @var $modal передається в цей фрагмент з того вигдялу де рендереться цей фрагмент */


?>
<?php $this->registerJs(
    "$(document).on('click','.my-modal-lg', function(e){
        //$('.#modal-lg-content').empty();
        //$('.modal-header').empty();
        e.preventDefault();
        $('#modal-lg').modal('show')
            .find('#modal-lg-content')
                .load($(this)
                    .attr('href'));
    });"); ?>
<?php
Modal::begin([
    'id'     => 'modal-lg',
    'size'   => 'modal-lg',
    'header' => '',
]);
echo "<div id='modal-lg-content'></div>";
Modal::end();

// JS: Update response handling
$js = <<<JS
$("body").on("beforeSubmit", "form#my-modal-form", function () {
                var form = $(this);
                // return false if form still have some validation errors
                if (form.find(".has-error").length) {
                    return false;
                }
                // submit form
                $.ajax({
                    url    : form.attr("action"),
                    type   : "post",
                    data   : form.serialize(),
                    success: function (response) {
                        $("#modal-lg").modal("toggle");
                        $.pjax.reload({container:"#lessons-grid-container-id"}); //for pjax update
                    },
                    error  : function () {
                        console.log("internal server error");
                    }
                });
                return false;
             });

// вібираем целевой єлемент
var target = document.getElementById('modal-lg-content');
 
// создаём єкземпляр MutationObserver
var observer = new MutationObserver(function(mutations) {
  mutations.forEach(function(mutation) {
   // console.log(mutation.type);
   var elem = $('#modal-lg-content').find('h1');
   elem.css({'float':'left', 'marginBottom':0 , 'marginTop':0})
   elem.appendTo('.modal-header');
  $('.redactor-editor').css({'width':' 99%'});
  $('.modal-body').css({'paddingTop':0});
  $('.modal-header').css({'border-bottom':0});
  });    
});
 
// конфигурация нашего observer:
var config = { attributes: true, childList: true, characterData: true };
 
// передаём в качестве аргументов целевой єлемент и его конфигурацию
observer.observe(target, config);
 
// позже можно остановить наблюдение
//observer.disconnect();

// розтягуємо модальне вікно по висоті контентг
$('#modal-lg').on('show.bs.modal', function () {
   // $('.modal .modal-body').css('overflow-y', 'auto'); 
    $('.modal .modal-body').css('max-height', 40000);
    $('.modal-content').css('background-color', '#E9E9E9');
  //  $('.modal .modal-body').css('min-height', $(window).height() * 0.5);
});

// закрити модальне вікно
  $(document).on('click', '.my-modal-lg', function () {
        
        $('.modal-header').html('<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>');
        
    });

JS;


$css = <<<CSS
    /*Модальні вікна*/

@media (min-width: 1400px) {
    .modal-lg {
        width: 1200px;
    }
}
CSS;


$this->registerCss($css);
$this->registerJs($js);


?>
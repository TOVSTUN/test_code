<div class="modal fade" id="get-modal-task-form" aria-hidden="true" aria-labelledby="task-modal-label"
     role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg-700">
        <div class="modal-content modal-task-form" id="modalContentTask">

        </div>
    </div>
</div>

<?php

$js = <<<JS
$(document).on('click', '.get-modal-task-form', function (e) {
    $('#modalContentTask').empty();    
    e.preventDefault();
    $('#get-modal-task-form').modal('show')
        .find('#modalContentTask')
        .load($(this)
            .attr('href'));
});
JS;


$this->registerJs($js);
?>
<?php
/**
 * Created by PhpStorm.
 * User: tovstun
 * Date: 29.04.18
 * Time: 10:55
 */

namespace backend\layouts\inspira\components;


use common\entities\Admin;
use common\entities\Company;
use common\helpers\StringHelper;
use common\statuses\MyStatus;

class LeftMenuGenerator extends LeftMenuHelper {
    public $systemName;
    private $menuList;
    private $itemsString;
    private $companies;
    private $admin;


    public function __construct(Admin $admin, $company = null){

        $this->companies = $company;
        $this->admin = $admin;
    }


    public function zgenerujMenu($menu){


        if(isset($menu->items_string)){
            $this->itemsString = StringHelper::convertToArray($menu->items_string);
        }

        $this->menuList = \common\entities\MenuItem::find()
            ->indexBy('id')
            ->where([
                'status' => MyStatus::STATUS_ACTIVE,
                'id'     => StringHelper::convertToSimpleArray($menu->items_string)])
            ->limit(500)
            ->all();


        if($this->itemsString && count($this->itemsString) >= 1){
            $menuItemsList = $this->zgenerujLevel1($this->itemsString);
        }

        $menu_header = '';

        $menu_name = mb_strtoupper($menu->name);
        //  print_r($menu_name); exit();
        //var_dump(); exit();
        if($menu->name_visibility == MyStatus::STATUS_ACTIVE){
            // s0 ДОРОБИТИ  По кліку по меню хідер порібно згорнути весь блок меню.і всі блоки меню виводити в згорнутому положенні (додай клас hide до блока меню, і логіку що коли ми клікаємо по заголовку нижчележачій блок розгортається в сі інші блоки меню ховаються, ато скоро можна буде в меню загубитися)
            $menu_header = " 
                          <ul class=\"nav metismenu\">                           
                                <li class=\"special_link\">
                                    <a style='text-align: center'>                                     
                                        <span class=\"nav-label\" >
                                            $menu_name
                                        </span>
                                    </a>
                                </li>
                          </ul>";
        }
        return $menu_header . $menuItemsList;
    }

    private function zgenerujLevel1($i){
        $li1 = '';
        foreach($i as $v){
            list($url, $name, $active, $icon, $status) = $this->parametryMenu($v);


            $li1 .= "<li class='" . $this->isItemActive($url) . "'> 
                                <a href='$url'>
                                    <i class='fa $icon'></i>
                                    <span class='nav-label'> $name </span>
                                    <span class='fa arrow'></span>
                                </a>";
            if(isset($v->children)) $li1 .= $this->zgenerujLevel2($v->children);
            $li1 .= "</li>";

        };
        return "<ul class='nav metismenu' id='side-menu'>" . $li1 . "</ul> ";
    }

    /**
     * @param $v
     * @return array
     */
    private function parametryMenu($v){
        $v = (object)$v;

        if(!empty($this->menuList[$v->id])){
            $url = $this->menuList[$v->id]->url;
            $icon = $this->menuList[$v->id]->icon;
            $name = $this->menuList[$v->id]->name;
            $status = $this->menuList[$v->id]->status;
            // $active = $this->active($v->id);
            return [$url, $name, null, $icon, $status];
        }
        return [null, null, null, null, null];
    }


    private function zgenerujLevel2($i){
        $li2 = '';
        foreach($i as $v){
            list($url, $name, $active, $icon, $status) = $this->parametryMenu($v);

            if($status == MyStatus::STATUS_ACTIVE){
                $li2 .= "<li class='" . $this->isItemActive($url) . "'> 
                                <a href='$url'>$name                                
                                    <span class='fa arrow'></span>
                                </a>";
                if(isset($v->children)) $li2 .= $this->zgenerujLevel3($v->children);
                $li2 .= "</li>";
            }
        }

        return "<ul class='nav nav-second-level collapse'>$li2</ul> ";
    }

    private function zgenerujLevel3($i){
        $li3 = '';
        foreach($i as $v){

            list($url, $name, $active, $icon, $status) = $this->parametryMenu($v);
            if($status == MyStatus::STATUS_ACTIVE){
                $li3 .= "<li class='" . $this->isItemActive($url) . "'><a href='$url'>$name</a></li>";
            }
        }
        return "<ul class='nav nav-third-level'> $li3 </ul>";
    }

    private function generateCompanyList(){

        $generator = new LeftMenuCompanyGenerator();
        return $generator->run($this->admin, $this->companies);
    }

    private function generateInputSearch(){
        return '<li style="padding-left: 20px;"><input id="menuSearch"></li>';
    }


}


<?php
/**
 * Created by PhpStorm.
 * User: tovstun
 * Date: 17.08.19
 * Time: 12:10
 */

namespace backend\layouts\inspira\components;


use Yii;

abstract class LeftMenuHelper {

    /** визначаємо активний пункт меню
     * @param $key
     * @return string
     */
    protected function isItemActive($key){

        $url = explode('?', Yii::$app->request->url);
        $url[0] = rtrim($url[0], '/');
        if(is_array($key)){
            foreach($key as $value){
                if($url[0] == $value) return 'active';
            }
        }
        if($url[0] == $key) return 'active';
        return '';

    }
}
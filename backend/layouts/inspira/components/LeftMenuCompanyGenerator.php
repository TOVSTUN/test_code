<?php
/**
 * Created by PhpStorm.
 * User: tovstun
 * Date: 17.08.19
 * Time: 11:58
 */

namespace backend\layouts\inspira\components;


use common\entities\Admin;
use common\entities\Company;
use common\helpers\MainHelper;

/**
 * Клас фабрика яка поверне згенероване ліве меню компанії відповідно до статусу та ролі юзера. тобто для адміна буде одне меню для менеджера зовсім друге
 */
class LeftMenuCompanyGenerator extends LeftMenuHelper {

    public function run($admin, $companies){

        if($admin->role_id == Admin::IS_ROOT || $admin->role_id == Admin::IS_MANAGER) return $this->forAdmin($companies);
        elseif($admin->role_id == Admin::IS_ACCOUNTANT) return $this->forManager($companies);
        elseif($admin->role_id == Admin::IS_MAGAZYNIER) return $this->forMadazynier($companies);
        else return null;
    }

    private function forAdmin($companies){
//S0 Структура цього файла потребуе глибокого рефвкторингу, потрібно винести всі повтори в окремий метод генратр, бо тут ацкій копіпаст
        $a = '';

        foreach($companies as $company){

            $url = "/company/{$company->id}/worker";

            $konfig = [
                'access-prosave' => "/access/prosave/{$company->id}",
                'access-company' => "/access/company/{$company->id}",
                'position'       => "/company/{$company->id}/position",
                'product-group'  => "/product-group/{$company->id}",
            ];

            $raporty = [
                'document'  => "/company/{$company->id}/document",
                'wydania'   => "/received-products/{$company->id}",
                'magazyn'   => "/warehouse/{$company->id}",
                'needs'     => "/needs/{$company->id}",
                'defective' => "/defective-products/{$company->id}",
                'washing'   => "/washing-raport/{$company->id}",

            ];

            $infra = [
                'location'   => "/company/{$company->id}/location",
                'building'   => "/company/{$company->id}/building",
                'department' => "/company/{$company->id}/department",
                'team'       => "/team/index/{$company->id}",
                'locker'     => "/locker/{$company->id}",
            ];

            $other = [
                'worker'    => "/company/{$company->id}/worker",
                'info'      => "/company/{$company->id}/info",
                'warehouse' => "/warehouse/{$company->id}",
                'absence'   => "/absence/{$company->id}",

            ];

            $washing = [
                'washing-local' => "/washing-local/{$company->id}",
                'washing-in'    => "/washing-local-doc/new-doc/{$company->id}",
                'washing-out'   => "/washing-local-doc/getting-products/{$company->id}",
            ];

            if($company->id == MainHelper::getCurrentCompanyId()){

                $button_washing = '';
//print_r($company->washing_company_id); exit;
                if(!empty($company->washing_company_id) && $company->washing_company_id > 0){
                    $button_washing = ' <li class="' . $this->isItemActive($washing) . '">                        
                        <a href="#">Pranie <span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level collapse" aria-expanded="false">
                                <li class="' . $this->isItemActive($washing['washing-local']) . '"><a href="' . $washing['washing-local'] . '">Historia </a></li>
                                <li class="' . $this->isItemActive($washing['washing-in']) . '"><a href="' . $washing['washing-in'] . '">Wyślij </a></li>
                                <li class="' . $this->isItemActive($washing['washing-out']) . '"><a href="' . $washing['washing-out'] . '">Odbierz </a></li>

                            </ul>
                        </li>';
                }


                $b = '<li class="active" style="margin-bottom: 30px">
                        <a href="' . $url . '"  aria-expanded="false">
                            <i class=\'fa $icon\'></i>
                            <span class="nav-label">' . $company->name . '</span>
                            <span class="fa arrow"></span>
                        </a>
                        
                        
                        <ul class="nav nav-second-level collapse" aria-expanded="true" style="">
                        <li class="' . $this->isItemActive($konfig) . '">
                            <a href="#">Konfiguracja <span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level collapse" aria-expanded="false">
                                <li><a class="my-modal-lg" href="/control/company/update?id=' . $company->id . '">Ustawienia firmy</a></li>
                                <li class="' . $this->isItemActive($konfig['access-prosave']) . '"><a href="' . $konfig['access-prosave'] . '">Dostęp dla Prosave</a></li>
                                <li class="' . $this->isItemActive($konfig['access-company']) . '"><a href="' . $konfig['access-company'] . '">Dostęp dla klienta</a></li>
                                <li class="' . $this->isItemActive($konfig['position']) . '"><a href="' . $konfig['position'] . '">Stanowiska pracy</a></li>
                                <li class="' . $this->isItemActive($konfig['product-group']) . '"><a  href="' . $konfig['product-group'] . '">Kod kreskowy</a></li>
                                <li class="' . $this->isItemActive($other['info']) . '"><a href="' . $other['info'] . '">O firmie </a></li>


                            </ul>
                        </li>
                        <li class="' . $this->isItemActive($raporty) . '">
                            <a href="#">Raporty <span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level collapse" aria-expanded="false">
                                <li class="' . $this->isItemActive($raporty['document']) . '"><a href="' . $raporty['document'] . '">Dokumenty wydania</a></li>
                                <li class="' . $this->isItemActive($raporty['wydania']) . '"><a href="' . $raporty['wydania'] . '">Raport wydań</a></li>
                                <li class="' . $this->isItemActive($raporty['magazyn']) . '"><a href="' . $raporty['magazyn'] . '">Magazyn</a></li>
                                <!--li class="' . $this->isItemActive($raporty['needs']) . '"><a href="' . $raporty['magazyn'] . '">Zapotrzebowanie</a></li-->
                                <li class="' . $this->isItemActive($raporty['defective']) . '"><a href="' . $raporty['defective'] . '">Reklamacje towarów</a></li>
                                <li class="' . $this->isItemActive($raporty['washing']) . '"><a href="' . $raporty['washing'] . '">Raport prania</a></li>


                            </ul>
                        </li>
                         <li class="' . $this->isItemActive($infra) . '">
                            <a href="#">Infrastruktura <span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level collapse" aria-expanded="false">
                                <li class="' . $this->isItemActive($infra['location']) . '"><a href="' . $infra['location'] . '">Lokalizacja</a></li>
                                <li class="' . $this->isItemActive($infra['building']) . '"><a href="' . $infra['building'] . '">Budynki</a></li>                      
                                <li class="' . $this->isItemActive($infra['department']) . '"><a href="' . $infra['department'] . '">Działy</a></li>
                                <li class="' . $this->isItemActive($infra['team']) . '"><a href="' . $infra['team'] . '">Brygady</a></li>
                                <li class="' . $this->isItemActive($infra['locker']) . '"><a href="' . $infra['locker'] . '">Szafki</a></li>
                            </ul>
                        </li>
                        
                        
                        <li class="' . $this->isItemActive($other['absence']) . '"><a href="' . $other['absence'] . '">Nieobecność </a></li>
                        <li class="' . $this->isItemActive($other['worker']) . '"><a href="' . $other['worker'] . '">Pracownicy </a></li>
                        <li class="' . $this->isItemActive($other['warehouse']) . '"><a href="' . $other['warehouse'] . '">Magazyn </a></li>
                        
                        
                       ' . $button_washing . '
                        
                    </ul>
                       
                        
                  </li>                                   
                  ';

                $a = $b . $a;

            }
            else{
                $a = $this->generateMenuItem($company, $url, $a);
            }
        }
        return $a;
    }

    private function generateMenuItem($company, $url, $a){

        if($company->status == Company::IS_NEW_COMPANY){
            $a .= '<li class="' . $this->isItemActive($url) . '  menu-company">
                        <a class="my-modal-lg" href="/control/company/update?id=' . $company->id . '"  aria-expanded="false">
                            <span class="nav-label nazwa" style="color:#FFFB00"> ' . $company->name . '</span>                            
                        </a>
                   </li>';
        }
        else{
            $a .= '<li class="' . $this->isItemActive($url) . ' menu-company">
                        <a href="' . $url . '"  aria-expanded="false">
                            <span class="nav-label nazwa">' . $company->name . '</span>
                            <span class="fa arrow"></span>
                        </a>
                   </li>';
        }
        return $a;
    }

    private function forManager($companies){

        $a = '';

        foreach($companies as $company){

            $url = "/company/{$company->id}/worker";

            $konfig = [
                // 'access-prosave' => "/access/prosave/{$company->id}",
                'position' => "/company/{$company->id}/position",
            ];

            $raporty = [
                'document' => "/company/{$company->id}/document",
                'wydania'  => "/received-products/{$company->id}",
                'magazyn'  => "/warehouse/{$company->id}",
                'needs'    => "/needs/{$company->id}",
            ];

            $infra = [
                'location'   => "/company/{$company->id}/location",
                'building'   => "/company/{$company->id}/building",
                'department' => "/company/{$company->id}/department",
                'team'       => "/team/index/{$company->id}",
                'locker'     => "/locker/{$company->id}",

            ];

            $other = [
                'worker'    => "/company/{$company->id}/worker",
                'info'      => "/company/{$company->id}/info",
                'warehouse' => "/warehouse/{$company->id}",
                'absence'   => "/absence/{$company->id}",
            ];


            if($company->id == MainHelper::getCurrentCompanyId()){

                $b = '<li class="active" style="margin-bottom: 30px">
                        <a href="' . $url . '"  aria-expanded="false">
                            <i class=\'fa $icon\'></i>
                            <span class="nav-label">' . $company->name . '</span>
                            <span class="fa arrow"></span>
                        </a>
                        
                        
                        <ul class="nav nav-second-level collapse" aria-expanded="true" style="">
                        <li class="' . $this->isItemActive($konfig) . '">
                            <a href="#">Konfiguracja <span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level collapse" aria-expanded="false">
                                <li><a href="#">Dostęp dla klienta</a></li>
                                <li class="' . $this->isItemActive($konfig['position']) . '"><a href="' . $konfig['position'] . '">Stanowiska pracy</a></li>
                                <li class="' . $this->isItemActive($other['info']) . '"><a href="' . $other['info'] . '">O firmie </a></li>


                            </ul>
                        </li>
                        <li class="' . $this->isItemActive($raporty) . '">
                            <a href="#">Raporty <span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level collapse" aria-expanded="false">
                                <li class="' . $this->isItemActive($raporty['document']) . '"><a href="' . $raporty['document'] . '">Dokumenty wydania</a></li>
                                <li class="' . $this->isItemActive($raporty['wydania']) . '"><a href="' . $raporty['wydania'] . '">Raport wydań</a></li>
                                <li class="' . $this->isItemActive($raporty['magazyn']) . '"><a href="' . $raporty['magazyn'] . '">Magazyn</a></li>
                                <li class="' . $this->isItemActive($raporty['needs']) . '"><a href="' . $raporty['magazyn'] . '">Zapotrzebowanie</a></li>

                            </ul>
                        </li>
                         <li class="' . $this->isItemActive($infra) . '">
                            <a href="#">Infrastruktura <span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level collapse" aria-expanded="false">
                                <li class="' . $this->isItemActive($infra['location']) . '"><a href="' . $infra['location'] . '">Lokalizacja</a></li>
                                <li class="' . $this->isItemActive($infra['building']) . '"><a href="' . $infra['building'] . '">Budynki</a></li>                        
                                <li class="' . $this->isItemActive($infra['department']) . '"><a href="' . $infra['department'] . '">Działy</a></li>
                                <li class="' . $this->isItemActive($infra['team']) . '"><a href="' . $infra['team'] . '">Brygady</a></li>
                                <li class="' . $this->isItemActive($infra['locker']) . '"><a href="' . $infra['locker'] . '">Szafki</a></li>

                            </ul>
                        </li>
                        
                        <li class="' . $this->isItemActive($other['absence']) . '"><a href="' . $other['absence'] . '">Nieobecność </a></li>
                        <li class="' . $this->isItemActive($other['worker']) . '"><a href="' . $other['worker'] . '">Pracownicy </a></li>
                        <li class="' . $this->isItemActive($other['warehouse']) . '"><a href="' . $other['warehouse'] . '">Magazyn </a></li>
                        
                    </ul>
                       
                        
                  </li>                                   
                  ';

                $a = $b . $a;

            }
            else{
                $a = $this->generateMenuItem($company, $url, $a);
            }
        }
        return $a;
    }

    private function forMadazynier($companies){

        $a = '';

        foreach($companies as $company){

            $url = "/company/{$company->id}/worker";

            $konfig = [

            ];

            $raporty = [
                'document' => "/company/{$company->id}/document",
                'wydania'  => "/received-products/{$company->id}",
                'magazyn'  => "/warehouse/{$company->id}",
                'needs'    => "/needs/{$company->id}",
            ];

            $infra = [
                'location'   => "/company/{$company->id}/location",
                'building'   => "/company/{$company->id}/building",
                'department' => "/company/{$company->id}/department",
                'team'       => "/team/index/{$company->id}",
                'locker'     => "/locker/{$company->id}",

            ];

            $other = [
                'worker'    => "/company/{$company->id}/worker",
                'info'      => "/company/{$company->id}/info",
                'warehouse' => "/warehouse/{$company->id}",
                'absence'   => "/absence/{$company->id}",
            ];


            if($company->id == MainHelper::getCurrentCompanyId()){

                $b = '<li class="active" style="margin-bottom: 30px">
                        <a href="' . $url . '"  aria-expanded="false">
                            <i class=\'fa $icon\'></i>
                            <span class="nav-label">' . $company->name . '</span>
                            <span class="fa arrow"></span>
                        </a>
                        
                        
                        <ul class="nav nav-second-level collapse" aria-expanded="true" style="">
                       
                        <li class="' . $this->isItemActive($raporty) . '">
                            <a href="#">Raporty <span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level collapse" aria-expanded="false">
                                <li class="' . $this->isItemActive($raporty['document']) . '"><a href="' . $raporty['document'] . '">Dokumenty wydania</a></li>
                                <li class="' . $this->isItemActive($raporty['wydania']) . '"><a href="' . $raporty['wydania'] . '">Raport wydań</a></li>
                                <li class="' . $this->isItemActive($raporty['magazyn']) . '"><a href="' . $raporty['magazyn'] . '">Magazyn</a></li>
                                <li class="' . $this->isItemActive($raporty['needs']) . '"><a href="' . $raporty['magazyn'] . '">Zapotrzebowanie</a></li>

                            </ul>
                        </li>
                         <li class="' . $this->isItemActive($infra) . '">
                            <a href="#">Infrastruktura <span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level collapse" aria-expanded="false">
                                <li class="' . $this->isItemActive($infra['location']) . '"><a href="' . $infra['location'] . '">Lokalizacja</a></li>
                                <li class="' . $this->isItemActive($infra['building']) . '"><a href="' . $infra['building'] . '">Budynki</a></li>                        
                                <li class="' . $this->isItemActive($infra['department']) . '"><a href="' . $infra['department'] . '">Działy</a></li>
                                <li class="' . $this->isItemActive($infra['team']) . '"><a href="' . $infra['team'] . '">Brygady</a></li>
                                <li class="' . $this->isItemActive($infra['locker']) . '"><a href="' . $infra['locker'] . '">Szafki</a></li>

                            </ul>
                        </li>
                        
                        <li class="' . $this->isItemActive($other['absence']) . '"><a href="' . $other['absence'] . '">Nieobecność </a></li>
                        <li class="' . $this->isItemActive($other['worker']) . '"><a href="' . $other['worker'] . '">Pracownicy </a></li>
                        <li class="' . $this->isItemActive($other['warehouse']) . '"><a href="' . $other['warehouse'] . '">Magazyn </a></li>
                        
                    </ul>
                       
                        
                  </li>                                   
                  ';

                $a = $b . $a;

            }
            else{
                $a = $this->generateMenuItem($company, $url, $a);
            }
        }
        return $a;
    }
}
<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\entities\Document */

$this->title = 'Rejestracja nowej umowy Nr' . $model->number;

?>
<div class="document-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

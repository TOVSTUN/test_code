<?php

use common\entities\Company;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\entities\search\DocumentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Umowy';
//$this->params['breadcrumbs'][] = $this->title;

$companiesFilter = Company::getSimpleFilter();
$acronimFilter = Company::getAllAcronims();

$this->params['url'] = 'advances_form';

$category = \common\entities\Category::getSimpleFilter();
$types = \common\entities\Type::getSimpleFilter();


?>
    <div class="document-index">

        <p>
            <?= Html::a('Dodaj nową umowe', ['create'], ['class' => 'btn  btn-sm btn-success my-modal-md']) ?>
            <?php echo Html::a('Usuń filtry', '/document', ['class' => 'btn btn-sm btn-info']) ?>
        </p>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel'  => $searchModel,
            'options'      => ['style' => 'table-layout:fixed; width:1300px'],

            'rowOptions' => function($model, $key, $index, $grid){


                $marker = '';

                if($model->date_end - (2592000 * 3) < time()) $marker = 'background-color: #fcffc7'; // за 3 місяці кінець умови
                if($model->date_end - 2592000 < time()) $marker = 'background-color: #ffc7c7'; // за 30 днів кінець умови
                if($model->status == \common\statuses\MyStatus::STATUS_INACTIVE) $marker = 'background-color: #d4d4d4'; // умова заерита та списана до архіву


                return ['style' => $marker];

            },
            'columns'    => [
                ['class' => 'yii\grid\SerialColumn', 'contentOptions' => ['style' => 'width: 40px;']],
                [
                    'contentOptions' => ['style' => 'width: 80px;'],
                    'label'          => 'Numer umowy',
                    'format'         => 'raw',
                    'attribute'      => 'number',
                    'value'          => function($data){
                        return '<a title="' . $data->description . '">' . $data->number . '</a>';
                    },
                ],

                [
                    'contentOptions' => ['style' => 'width: 100px; '],
                    'label'          => 'Akronim',
                    'format'         => 'raw',
                    'attribute'      => 'company_acronim',
                    'value'          => function($data){
                        return Html::a(
                            $data->company->acronim,
                            '/document/index?DocumentSearch%5Bcompany_id%5D=' . $data->company_id
                        );
                    },
                ],

                [
                    'contentOptions' => ['style' => 'width: 150px;'],
                    'label'          => 'Nazwa firmy',
                    'format'         => 'raw',
                    'attribute'      => 'company_name',
                    'value'          => function($data){
                        return Html::a(
                            $data->company->name,
                            '/document/index?DocumentSearch%5Bcompany_id%5D=' . $data->company_id
                        );
                    },
                ],
                [
                    'contentOptions' => ['style' => 'width: 60px;'],
                    'label'          => 'Kategoria',
                    'format'         => 'raw',
                    'attribute'      => 'category_id',
                    'filter'         => $category,
                    'value'          => function($data){
                        return Html::a(
                            $data->category->name ?? ''
                        );
                    },
                ],
                /*     [
                         'contentOptions' => ['style' => 'width: 100px; max-width: 100px;', ],
                         'label'          => 'Typ kontrahenta',
                         'format'         => 'raw',
                         'attribute'      => 'type_id',
                         'filter'         => $types,
                         'value'          => function($data){
                             return Html::a(
                                 $data->type->name ?? ''
                             );
                         },
                     ],*/


                /*  [
                      'contentOptions' => ['style' => 'width: 200px; max-width: 200px;'],
                      'label'          => 'Nazwa umowy',
                      'format'         => 'raw',
                      'attribute'      => 'name',
                  ],*/
                /*  [
                      'contentOptions' => ['style' => 'width: 110px;'],
                      'label'          => 'Data zawarcia',
                      'format'         => 'raw',
                      'attribute'      => 'date_start_str',
                  ],*/
                [
                    'contentOptions' => ['style' => 'width: 100px;'],
                    'label'          => 'Data zawarcia',
                    'format'         => 'raw',
                    'attribute'      => 'date_init_str',
                ],
                [
                    'contentOptions' => ['style' => 'width: 100px;'],
                    'label'          => 'Data zakończenia',
                    'format'         => 'raw',
                    'attribute'      => 'date_end_str',
                    'value'          => function($data){
                        if($data->date_end == 2147483647){
                            return 'czas nieokreślony';
                        }
                        return $data->date_end_str;
                    },
                ],


                ['contentOptions' => ['style' => 'width: 100px;'],
                 'format'         => 'raw',
                 'value'          => function($data){
                     $url = '/document/view?id=' . $data->id;
                     return Html::a('Podgłąd', $url, ['class' => "my-modal-lg btn btn-info btn-xs",
                                                      'title' => $data->description,
                     ]);
                 },
                ],
            ],
        ]); ?>
    </div>
<?php
$js = <<<'TAG'
$(document).ready(function () { 

    $(document).on('click', '#radio', function (e) {
        
        var value = $('input[name="radio"]:checked').val();
        if(value == 0) {
            $('#period').removeClass('hide');            
        }
        if(value == 10) {
            $('#period').addClass('hide');
            $('#document-date_end_str').val('');
            
        }
     });
});
TAG;
$this->registerJs('$(".chosen-select").chosen({width: "100%"});');
$this->registerJs($js, $this::POS_READY);


?>
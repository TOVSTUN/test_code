<?php

use common\entities\File;
use common\entities\Helper;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\entities\Document */
/* @var $form yii\widgets\ActiveForm */

$categories = \common\entities\Category::getSimpleFilter();
$types = \common\entities\Type::getSimpleFilter();
$periods = \common\entities\Period::getSimpleFilter();

?>

    <div id="form" class="document-form">

        <?php $form = ActiveForm::begin([
            'id'      => 'document-form',
            //'enableAjaxValidation' => true,
            'options' => ['enctype' => 'multipart/form-data'],
        ]); ?>

        <div class="row">
            <div class="col-sm-6">

                <?= $form->field($model, 'company_id')
                    ->textInput()
                    ->dropDownList(\yii\helpers\ArrayHelper::map(\common\entities\Company::find()->all(), 'id', 'name'),
                        [
                            'class'  => 'chosen-select form-control',
                            'prompt' => 'Wyszukaj firmę',
                        ])
                    ->label('Nazwa firmy')

                ?>


            </div>
            <div class="col-sm-6">

                <?= Html::a('Nie ma firmy? Dodaj firmę', '/company/create', ['class' => 'btn btn-primary my-modal-md', 'style' => 'margin-top:22px']) ?>
            </div>
        </div>


        <?php // $form->field($model, 'name')->textInput(['maxlength' => true])->label('Nazwa umowy') ?>

        <?= $form->field($model, 'category_id')->textInput()->dropDownList($categories, ['prompt' => '-- BRAK --'])->label('Nazwa kategorii') ?>
        <?= $form->field($model, 'type_id')->textInput()->dropDownList($types, ['prompt' => '-- BRAK --'])->label('Typ kontrahenta') ?>
        <?= $form->field($model, 'period_id')->textInput()->dropDownList($periods, ['prompt' => '-- BRAK --'])->label('Okres wypowiedzenia') ?>
        <?= $form->field($model, 'admin_id')
            ->textInput()
            ->dropDownList(\common\entities\Admin::getAllUser(), [
                'class'  => 'chosen-select',
                'prompt' => '-- BRAK --'])
            ->label('Opiekun')
        ?>
        <?= $form->field($model, 'date_init_str')->widget(DatePicker::className(), [
            'name'          => 'dp_1',
            'type'          => DatePicker::TYPE_INPUT,
            'options'       => ['placeholder' => 'Data zawarcia umowy/aneksu'],
            'convertFormat' => true,
            'pluginOptions' => [
                'format'    => 'yyyy-MM-dd',
                'autoclose' => true,
                'weekStart' => 1, //неделя начинается с понедельника
                'startDate' => '01.05.2018', //самая ранняя возможная дата
                'todayBtn'  => false, //снизу кнопка "сегодня"
                'style'     => 'color:red',
            ],
        ])->label('Data zawarcia umowy/aneksu'); ?>
        <?= $form->field($model, 'date_start_str')->widget(DatePicker::className(), [
            'name'          => 'dp_2',
            'type'          => DatePicker::TYPE_INPUT,
            'options'       => ['placeholder' => 'Umowa rozpoczięta'],
            'convertFormat' => true,
            //'value' => date("d.m.Y", (integer)$model->date_end),
            'pluginOptions' => [
                'format'    => 'yyyy-MM-dd',
                'autoclose' => true,
                'weekStart' => 1, //неделя начинается с понедельника
                'startDate' => '01.05.2018', //самая ранняя возможная дата
                'todayBtn'  => false, //снизу кнопка "сегодня"
            ],
        ])->label('Data rozpoczęcia umowy'); ?>
        <div id="radio">
            <p>Czas trwania umowy:</p>

            <div>
                <input type="radio" id="time-nieok" name="radio" value="10">
                <label for="dewey">Nieokreślony</label>
            </div>

            <div>
                <input type="radio" id="time-ok" name="radio" value="0" checked>
                <label for="huey">Określony</label>
            </div>
        </div>
        <div id="period">
            <?= $form->field($model, 'date_end_str')->widget(DatePicker::className(), [
                'name'          => 'dp_3',
                'type'          => DatePicker::TYPE_INPUT,
                'options'       => ['placeholder' => 'Data zakończenia umowy'],
                'convertFormat' => true,
                //'value' => date("d.m.Y", (integer)$model->time_payment_str),
                'pluginOptions' => [
                    'format'    => 'yyyy-MM-dd',
                    'autoclose' => true,
                    'weekStart' => 1, //неделя начинается с понедельника
                    'startDate' => '01.05.2018', //самая ранняя возможная дата
                    'todayBtn'  => false, //снизу кнопка "сегодня"
                ],
            ])->label('Data zakończenia umowy'); ?>
        </div>
        <?= $form->field($model, 'description')->textarea(['rows' => 5])->label('Przedmiot umowy') ?>
        <?= $form->field($model, 'imageFile[]')
            ->fileInput(['multiple' => true])
            ->label('Dodaj nowe pliki') ?>
        <?php

        if($model->id > 1){
            $files = File::find()->where(['document_id' => $model->id])->all();
            $a = '';
            foreach($files as $file){
                $a .= "<a target='_blank' 
                                  href='/uploads/$file->hash' 
                                  download='$file->name'>$file->name$file->extension</a> 
                                    - 
                               <a target='_blank' href='/document/remove-file/$file->document_id/$file->hash'> usuń </a>
                               
                               <br>";
            }
            echo $a;
        }

        ?>

        <div class="form-group" style="text-align: right">

            <?= Html::a('Usuń', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'style' => 'margin-right:100px',
                'data'  => [
                    'confirm' => 'Naprawde chcesz usunąć ten document?',
                    'method'  => 'post',
                ],
            ]) ?>


            <?= Html::submitButton('Zapisz', ['class' => 'btn btn-success mybtn']) ?>
        </div>


        <?php ActiveForm::end(); ?>

    </div>
    <div>

        <div id="spinner" class="sk-spinner sk-spinner-fading-circle hide" style="width: 150px; height: 150px;">
            <div class="sk-circle1 sk-circle"></div>
            <div class="sk-circle2 sk-circle"></div>
            <div class="sk-circle3 sk-circle"></div>
            <div class="sk-circle4 sk-circle"></div>
            <div class="sk-circle5 sk-circle"></div>
            <div class="sk-circle6 sk-circle"></div>
            <div class="sk-circle7 sk-circle"></div>
            <div class="sk-circle8 sk-circle"></div>
            <div class="sk-circle9 sk-circle"></div>
            <div class="sk-circle10 sk-circle"></div>
            <div class="sk-circle11 sk-circle"></div>
            <div class="sk-circle12 sk-circle"></div>
        </div>

    </div>


<?php
$this->registerJs('$(".chosen-select").chosen({width: "100%"});

$(".mybtn").click(function (){
    $("#form").addClass(\'hide\');
    $("#spinner").removeClass(\'hide\');
});

')
?>
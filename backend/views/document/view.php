<?php

use common\entities\File;
use common\statuses\MyStatus;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\entities\Document */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Documents', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="document-view">

    <h1>Podgląd umowy </h1>

    <div class="row">
        <div class="col-sm-6">
            <h2><?= $model->number . ' - ' . $model->company->name ?></h2>
            <?= DetailView::widget([
                'model'      => $model,
                'attributes' => [

                    [
                        //   'contentOptions' => ['style' => 'width: 200px; max-width: 200px;'],
                        'captionOptions' => ['width' => '25%'],
                        'contentOptions' => ['width' => '75%'],
                        'label'          => 'Numer umowy',
                        'format'         => 'raw',
                        'attribute'      => 'number',
                    ],
                    [
                        'label'     => 'Nazwa firmy',
                        'format'    => 'raw',
                        'attribute' => 'company_id',
                        'value'     => function($data){
                            return Html::a(
                                $data->company->name,
                                '/document/index?DocumentSearch%5Bcompany_id%5D=' . $data->company_id, []
                            );
                        },
                    ],
                    [
                        'contentOptions' => ['style' => 'width: 100px; max-width: 100px;',],
                        'label'          => 'Opiekun',
                        'format'         => 'raw',
                        'value'          => function($data){
                            return $data->admin->username ?? '';
                        },
                    ],
                    [
                        'contentOptions' => ['style' => 'width: 100px; max-width: 100px;'],
                        'label'          => 'Kategoria',
                        'format'         => 'raw',
                        'attribute'      => 'category_id',
                        'value'          => function($data){
                            return Html::a(
                                $data->category->name ?? ''
                            );
                        },
                    ],
                    [
                        'contentOptions' => ['style' => 'width: 100px; max-width: 100px;',],
                        'label'          => 'Typ kontrahenta',
                        'format'         => 'raw',
                        'attribute'      => 'type_id',
                        'value'          => function($data){
                            return Html::a(
                                $data->type->name ?? ''
                            );
                        },
                    ],

                    [
                        'contentOptions' => ['style' => 'width: 100px; max-width: 100px;',],
                        'label'          => 'Okres wypowiedzenia',
                        'format'         => 'raw',
                        'value'          => function($data){
                            return $data->period->name ?? '';
                        },
                    ],


                    /* [
                         //   'contentOptions' => ['style' => 'width: 200px; max-width: 200px;'],
                         'label'     => 'Nazwa umowy',
                         'format'    => 'raw',
                         'attribute' => 'name',
                     ],*/


                    [
                        'label'     => 'Data zawarcia umowy/aneksu',
                        'format'    => 'raw',
                        'attribute' => 'date_init_str',
                    ],
                    [
                        'label'     => 'Data rozpoczęcia umowy',
                        'format'    => 'raw',
                        'attribute' => 'date_start_str',
                    ],
                    [
                        'label'     => 'Data wygaśnięcia umowy',
                        'format'    => 'raw',
                        'attribute' => 'date_end_str',
                    ],

                    [
                        'label'     => 'Przedmiot umowy',
                        'format'    => 'raw',
                        'attribute' => 'description',
                    ],

                    [
                        'label'     => 'Status umowy',
                        'format'    => 'raw',
                        'attribute' => 'status',
                        'value'     => function($data){
                            return MyStatus::whatStatusDoc($data->status);
                        },
                    ],

                ],
            ]) ?>

        </div>
        <div class="col-sm-6">
            <h2>Załączniki</h2>
            <?php
            if($model->id > 1){
                $files = File::find()->where(['document_id' => $model->id])->all();
                $a = '';
                foreach($files as $file){
                    $a .= "<a target='_blank'
                      href='/uploads/$file->hash'
                      download='$file->name'>$file->name$file->extension</a><br>";
                }
                echo $a;
            }
            ?>

        </div>


    </div>


    <p style="text-align: right">
        <?php

        if($model->status == MyStatus::STATUS_ACTIVE){
            echo Html::a('Umowa zakończona', ['close', 'document_id' => $model->id], [
                'class' => 'btn btn-success',
                'style' => 'margin-right:100px',
                'data'  => [
                    'confirm' => 'Czy podtwiedzash wyslania do archiwum tej umowy?',
                    'method'  => 'post',
                ],
            ]);
            echo Html::a('Edytuj umowę', ['update', 'id' => $model->id], ['class' => 'btn btn-primary my-modal-lg']);


        }
        else{
            echo Html::a('Aktywój', ['activate', 'id' => $model->id], [
                'class' => 'btn btn-info',
                //'style' => 'margin-right:100px',
                'data'  => [
                    'confirm' => 'Naprawde chcesz aktywować ten document?',
                    'method'  => 'post',
                ],
            ]);

        }
        ?>


    </p>

</div>

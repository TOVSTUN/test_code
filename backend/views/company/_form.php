<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\entities\Company */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="company-form">

    <?php $form = ActiveForm::begin(['id' => 'company-form', 'enableAjaxValidation' => true]); ?>

    <?= $form->field($model, 'acronim')->textInput(['maxlength' => true])->label('Akronim') ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true])->label('Nazwa') ?>

    <?= $form->field($model, 'adress')->textInput(['maxlength' => true])->label('NIP kontrahenta') ?>

    <div class="form-group" style="text-align: right">
        <?= Html::submitButton('Zapisz', ['class' => 'btn btn-success ']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

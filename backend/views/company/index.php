<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\entities\search\CompanySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kontrahenci';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-index">

    <p>
        <?= Html::a('Dodaj nową firmę', ['create'], ['class' => 'btn btn-success btn-sm my-modal-md']) ?>
        <?= Html::a('Usuń filtry', ['/admin'], ['class' => 'btn btn-sm btn-info']) ?>

    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        // 'options' => [ 'style' => 'table-layout:fixed;' ],
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn', 'contentOptions' => ['style' => 'width: 50px;'],],
            [
                'attribute'      => 'acronim',
                'label'          => 'Akronim',
                'contentOptions' => ['style' => 'width: 200px;'],
                'format'         => 'raw',
            ],
            [
                'attribute'      => 'name',
                'label'          => 'Nazwa',
                'contentOptions' => ['style' => 'width: 300px;'],
                'format'         => 'raw',
                'value'          => function($data){

                    return Html::a($data->name, '/company/view?id=' . $data->id,
                        [
                            'title' => 'wizytówka kjijenta',
                            'class' => 'my-modal-md',
                            //'target' => '_blank',
                        ]);
                },
            ],
            [
                'attribute'      => 'adress',
                'label'          => 'NIP',
                'contentOptions' => ['style' => 'width: 130px;'],
                'format'         => 'raw',

            ],

            [
                'attribute'      => 'created_at',
                'label'          => 'Dodano',
                'contentOptions' => ['style' => 'width: 100px;'],
                'format'         => 'raw',
                'value'          => function($data){
                    return date('Y-m-d', $data->created_at);
                },
            ],

            // 'adress',
            // 'description:ntext',
            //  'created_at:date:Dodano',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn', 'buttons' => ['update' => function($url, $model){
                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['class' => 'my-modal-md', 'data-pjax' => '0']);
            }, 'view'                                                   => function($url, $model){
                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['class' => 'my-modal-md', 'data-pjax' => '0']);
            },],],
        ],
    ]); ?>


</div>

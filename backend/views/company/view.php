<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\entities\Company */

$this->title = $model->name;

\yii\web\YiiAsset::register($this);
?>
<div class="company-view">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= DetailView::widget([
        'model'      => $model,
        'attributes' => [
            'acronim:raw:Akronim',
            'name:raw:Nazwa',
            'adress:raw:NIP',

            [
                'attribute' => 'created_at',
                'label'     => 'Dodano',
                //'contentOptions' => ['style' => 'width: 100px;'],
                'format'    => 'raw',
                'value'     => function($data){
                    return date('Y-m-d', $data->created_at);
                },
            ],
            [
                'attribute' => 'updated_at',
                'label'     => 'Edytowano',
                //'contentOptions' => ['style' => 'width: 100px;'],
                'format'    => 'raw',
                'value'     => function($data){
                    return date('Y-m-d', $data->created_at);
                },
            ],

            //  'created_at:date:Dodano',
            //  'updated_at:date:Edytowano',
        ],
    ]) ?>

</div>

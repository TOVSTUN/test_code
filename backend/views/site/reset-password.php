<?php
/* @var $this yii\web\View */
$this->title = Yii::t('app', 'Zmiana hasła');

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Панель керування'), 'url' => ['/shop/dashboard']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="password-update">
    <h1>Zmiana hasła</h1>
    <?= /** @var \frontend\controllers\SettingsAdminController actionProfile() $model */
    $this->render('_reset_password_form', [
        'model' => $model,
    ]) ?>

</div>
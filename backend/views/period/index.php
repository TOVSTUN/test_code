<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\entities\search\PeriodSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Okres wypowiedzenia';
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="period-index">

    <p>
        <?= Html::a('Dodaj nowy okres wypowiedzenia', ['create'], ['class' => 'btn btn-success my-modal-md']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn', 'contentOptions' => ['style' => 'width: 80px; max-width: 80px;'],],

            [
                'contentOptions' => ['style' => 'width: 200px; max-width: 200px;'],
                'label'          => 'Nazwa',
                'format'         => 'raw',
                'attribute'      => 'name',
            ],

            ['class' => 'yii\grid\ActionColumn', 'buttons' => ['update' => function($url, $model){
                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['class' => 'my-modal-md', 'data-pjax' => '0']);
            }, 'view'                                                   => function($url, $model){
                return false;
            },],],
        ],
    ]); ?>
</div>

<?php
/**
 * Created by PhpStorm.
 * User: tovstun
 * Date: 08.03.19
 * Time: 14:21
 */

namespace common\entities;

use common\components\HtmlHelper;
use common\statuses\MyStatus;
use phpDocumentor\Reflection\Types\Self_;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;

/**
 *
 * @property-read mixed $authKey
 * @property-read mixed $allowedCompanies
 */
class Admin extends _Admin implements IdentityInterface {

    const IS_ROOT = 1;
    const IS_MANAGER = 2;
    const IS_ACCOUNTANT = 4;


    public $password;
    public $role_name; // тут лежить назва ролі наприклад Адмін. або мастер, або босс

    public function rules(){
        return array_merge(parent::rules(), [[['password'], 'string']]);
    }

    public function behaviors(){
        return [
            [
                'class'      => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }


    public function afterFind(){
        parent::afterFind();

        $this->role_name = $this->role->name;
    }

    public static function getAllUser(){
        return ArrayHelper::map(self::find()->all(), 'id', 'username');
    }

    public function getAllowedCategories(){

        $ids = '';

        if($this->role_id == self::IS_ROOT){
            $admin_access = Category::find()->all();
            $ids = ArrayHelper::getColumn($admin_access, 'id');
        }
        else{
            $ids = ArrayHelper::getColumn($this->getAdminHasCategories()->all(), 'category_id');
        }
        return $ids;
    }

    public function getAllowedTypes(){
        //  Dostawca, Odbiorca
        $ids = '';
        if($this->role_id == self::IS_ROOT || $this->role_id == self::IS_ACCOUNTANT){
            $admin_access = Type::find()->all();
            $ids = ArrayHelper::getColumn($admin_access, 'id');
        }
        else{
            $ids = ArrayHelper::getColumn($this->getAdminHasTypes()->all(), 'type_id');
        }
        return $ids;
    }

    /** Метод який повернеу айдішки компаній відкриті для юзера у форматі ['123', '345']
     *  Для юзера з групп зажонд поверне айдішки всіх  компаній
     */
    public function getAllowedCompanies(){

        $ids = '';

        if($this->role_id == self::IS_ROOT || $this->role_id == self::IS_MANAGER){
            $admin_access = Company::find()->all();
            $ids = ArrayHelper::getColumn($admin_access, 'id');
        }
        else{
            $admin_access = AdminHasCompany::findAll(['admin_id' => $this->id]);
            $ids = ArrayHelper::getColumn($admin_access, 'company_id');
        }
        return $ids;
    }

    public function switchAccessType($type_id){

        $com = AdminHasType::find()->where([
            'type_id'  => $type_id,
            'admin_id' => $this->id,
        ])->one();


        if($com && isset($com->admin_id)){
            $com->delete();
            return HtmlHelper::getOffIcon();
        }
        else{
            $com = new AdminHasType();
            $com->admin_id = $this->id;
            $com->type_id = $type_id;
            $com->save();
            return HtmlHelper::getOnIcon();
        }

    }

    public function switchAccessCategory($category_id){

        //if(in_array($company_id, self::getAccessCompany())){

        $com = AdminHasCategory::find()->where([
            'category_id' => $category_id,
            'admin_id'    => $this->id,
        ])->one();
//Yii::error($com,'dddddddddddddd');
        if($com && isset($com->admin_id)){
            $com->delete();
            return HtmlHelper::getOffIcon();
        }
        else{
            $com = new AdminHasCategory();
            $com->admin_id = $this->id;
            $com->category_id = $category_id;
            $com->save();
            return HtmlHelper::getOnIcon();
        }

        // return false;
    }

    public static function getAdminRole($role_id = Null){

        $role = [
            self::IS_ROOT       => 'Zarząd',
            self::IS_MANAGER    => 'Administrator',
            self::IS_ACCOUNTANT => 'Handlowiec',
        ];

        return $role_id ? $role[$role_id] : $role;

    }

    public function beforeSave($insert){

        if(!empty($this->password)) $this->setPassword($this->password);
        return parent::beforeSave($insert);
    }

    public static function findIdentity($id){
        return static::findOne(['id' => $id, 'status' => MyStatus::STATUS_ACTIVE]);
    }

    /**Admin
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null){
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername(string $username){
        return static::findOne(['username' => $username, 'status' => MyStatus::STATUS_ACTIVE]);
    }

    public static function findByEmail($email){
        return static::findOne(['email' => $email, 'status' => MyStatus::STATUS_ACTIVE]);
    }

    public static function findByKey($key_id){
        return static::findOne(['key_id' => $key_id, 'status' => MyStatus::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken(string $token){
        if(!static::isPasswordResetTokenValid($token)){
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status'               => MyStatus::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid(string $token){
        if(empty($token)){
            return false;
        }

        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId(){
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey(){
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey){
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword(string $password){
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     * @throws \yii\base\Exception
     */
    public function setPassword(string $password){
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey(){
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken(){
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken(){
        $this->password_reset_token = null;
    }


}
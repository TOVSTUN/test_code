<?php

namespace common\entities\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\entities\MenuItem;

/**
 * MenuItemSearch represents the model behind the search form of `common\entities\MenuItem`.
 */
class MenuItemSearch extends MenuItem {
    /**
     * {@inheritdoc}
     */
    public function rules(){
        return [
            [['id', 'status', 'global'], 'integer'],
            [['url', 'icon', 'ru_name', 'ua_name', 'pl_name', 'en_name', 'ru_description', 'ua_description', 'pl_description', 'en_description'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios(){
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params){
        $query = MenuItem::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if(!$this->validate()){
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id'     => $this->id,
            'status' => $this->status,
            'global' => $this->global,
        ]);

        $query->andFilterWhere(['like', 'url', $this->url])
            ->andFilterWhere(['like', 'icon', $this->icon])
            ->andFilterWhere(['like', 'ru_name', $this->ru_name])
            ->andFilterWhere(['like', 'ua_name', $this->ua_name])
            ->andFilterWhere(['like', 'pl_name', $this->pl_name])
            ->andFilterWhere(['like', 'en_name', $this->en_name])
            ->andFilterWhere(['like', 'ru_description', $this->ru_description])
            ->andFilterWhere(['like', 'ua_description', $this->ua_description])
            ->andFilterWhere(['like', 'pl_description', $this->pl_description])
            ->andFilterWhere(['like', 'en_description', $this->en_description]);

        return $dataProvider;
    }
}

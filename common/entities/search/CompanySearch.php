<?php

namespace common\entities\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\entities\Company;

/**
 * CompanySearch represents the model behind the search form of `common\entities\Company`.
 */
class CompanySearch extends Company {
    /**
     * {@inheritdoc}
     */
    public function rules(){
        return [
            [['id', 'created_at', 'updated_at'], 'integer'],
            [['acronim', 'name', 'adress', 'description', 'contact_info'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios(){
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params){
        $query = Company::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if(!$this->validate()){
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id'         => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'acronim', $this->acronim])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'adress', $this->adress])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'contact_info', $this->contact_info]);

        return $dataProvider;
    }
}

<?php

namespace common\entities\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\entities\Menu;

/**
 * MenuSearch represents the model behind the search form of `common\entities\Menu`.
 */
class MenuSearch extends Menu {
    /**
     * {@inheritdoc}
     */
    public function rules(){
        return [
            [['id', 'status', 'order', 'name_visibility', 'item_name_visibility', 'item_icon_visibility', 'old_items_string_update', 'role_id'], 'integer'],
            [['system_name', 'items_string', 'old_items_string', 'ru_name', 'ua_name', 'pl_name', 'en_name', 'ru_description', 'ua_description', 'pl_description', 'en_description'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios(){
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params){
        $query = Menu::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if(!$this->validate()){
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id'                      => $this->id,
            'status'                  => $this->status,
            'order'                   => $this->order,
            'name_visibility'         => $this->name_visibility,
            'item_name_visibility'    => $this->item_name_visibility,
            'item_icon_visibility'    => $this->item_icon_visibility,
            'old_items_string_update' => $this->old_items_string_update,
            'role_id'                 => $this->role_id,
        ]);

        $query->andFilterWhere(['like', 'system_name', $this->system_name])
            ->andFilterWhere(['like', 'items_string', $this->items_string])
            ->andFilterWhere(['like', 'old_items_string', $this->old_items_string])
            ->andFilterWhere(['like', 'ru_name', $this->ru_name])
            ->andFilterWhere(['like', 'ua_name', $this->ua_name])
            ->andFilterWhere(['like', 'pl_name', $this->pl_name])
            ->andFilterWhere(['like', 'en_name', $this->en_name])
            ->andFilterWhere(['like', 'ru_description', $this->ru_description])
            ->andFilterWhere(['like', 'ua_description', $this->ua_description])
            ->andFilterWhere(['like', 'pl_description', $this->pl_description])
            ->andFilterWhere(['like', 'en_description', $this->en_description]);

        return $dataProvider;
    }
}

<?php

namespace common\entities\search;

use common\entities\Company;
use common\helpers\MainHelper;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\entities\Document;
use yii\helpers\ArrayHelper;

/**
 * DocumentSearch represents the model behind the search form of `common\entities\Document`.
 */
class DocumentSearch extends Document {

    public $date_end_str2;
    public $company_id2;
    public $date_start_str2;
    public $allowedCategories;
    public $allowedTypes;
    public $company_name;
    public $company_acronim;


    public function __construct($categories, $types){

        $this->allowedCategories = $categories;
        $this->allowedTypes = $types;
        parent::__construct();
    }

    public function rules(){
        return [
            [['id', 'date_start', 'date_end', 'time_payment', 'time_delivery', 'company_id', 'company_id2', 'category_id', 'type_id'], 'integer'],
            [['files', 'description', 'name', 'number', 'date_start_str2', 'date_end_str2', 'date_start_str', 'date_end_str', 'date_init_str', 'company_name', 'company_acronim'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios(){
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params){
        $query = Document::find();

        if($this->company_id2){
            $query->andFilterWhere(['=', 'company_id', $this->company_id2]);
        }


        if($this->date_start_str2){
            $query->andWhere(['>=', 'date_start', MainHelper::decodeTime_Y_M_D($this->date_start_str2)]);
            $query->andWhere(['<=', 'date_start', MainHelper::decodeTime_Y_M_D($this->date_start_str2) + 86400]);
        }

        if($this->date_end_str2){
            $query->andWhere(['>=', 'date_end', MainHelper::decodeTime_Y_M_D($this->date_end_str2)]);
            $query->andWhere(['<=', 'date_end', MainHelper::decodeTime_Y_M_D($this->date_end_str2) + 86400]);
        }

        if($this->company_name){
            $company = Company::find()->where(['like', 'name', trim($this->company_name)])->all();
            $ids = ArrayHelper::getColumn($company, 'id');
            $query->andWhere(['IN', 'company_id', $ids]);
        }

        if($this->company_acronim){
            $company = Company::find()->where(['like', 'name', trim($this->company_acronim)])->all();
            $ids = ArrayHelper::getColumn($company, 'id');
            $query->andWhere(['IN', 'company_id', $ids]);
        }


        $query->andWhere(['IN', 'category_id', $this->allowedCategories]);
        $query->andWhere(['IN', 'type_id', $this->allowedTypes]);
        $query->orWhere(['created_admin_id' => \Yii::$app->user->identity->getId()]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if(!$this->validate()){
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id'            => $this->id,
            'date_start'    => $this->date_start,
            'date_end'      => $this->date_end,
            'time_payment'  => $this->time_payment,
            'time_delivery' => $this->time_delivery,
            'company_id'    => $this->company_id,
            'category_id'   => $this->category_id,
            'type_id'       => $this->type_id,
        ]);

        $query->andFilterWhere(['like', 'files', $this->files])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'number', $this->number])
            ->andFilterWhere(['=', 'company_id', $this->company_id2])
            // ->andFilterWhere(['like', 'number', $this->])
            ->andFilterWhere(['like', 'date_end_str', $this->date_end_str])
            ->andFilterWhere(['like', 'date_start_str', $this->date_start_str])
            ->andFilterWhere(['like', 'date_init_str', $this->date_init_str]);

        $query->orderBy([
            'status'   => SORT_DESC,
            'date_end' => SORT_ASC,
        ]);

        return $dataProvider;
    }
}

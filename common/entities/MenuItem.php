<?php
/**
 * Created by PhpStorm.
 * User: tovstun
 * Date: 08.03.19
 * Time: 14:31
 */

namespace common\entities;

use common\traits\MultiLanguage;

class MenuItem extends _MenuItem {

    use MultiLanguage;
}
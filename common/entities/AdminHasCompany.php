<?php

namespace common\entities;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "admin_has_company".
 *
 * @property int $admin_id
 * @property int $company_id
 *
 * @property Admin $admin
 * @property Company $company
 */
class AdminHasCompany extends ActiveRecord {
    /**
     * {@inheritdoc}
     */
    public static function tableName(){
        return 'admin_has_company';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(){
        return [
            [['admin_id', 'company_id'], 'required'],
            [['admin_id', 'company_id'], 'integer'],
            [['admin_id'], 'exist', 'skipOnError' => true, 'targetClass' => Admin::className(), 'targetAttribute' => ['admin_id' => 'id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(){
        return [
            'admin_id'   => 'Admin ID',
            'company_id' => 'Company ID',
        ];
    }

    /**
     * Gets query for [[Admin]].
     *
     * @return ActiveQuery
     */
    public function getAdmin(){
        return $this->hasOne(Admin::className(), ['id' => 'admin_id']);
    }

    /**
     * Gets query for [[Company]].
     *
     * @return ActiveQuery
     */
    public function getCompany(){
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }
}

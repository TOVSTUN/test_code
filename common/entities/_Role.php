<?php

namespace common\entities;

use Yii;

/**
 * This is the model class for table "role".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 *
 * @property Admin[] $admins
 * @property Menu[] $menus
 * @property RoleHasCategory[] $roleHasCategories
 * @property RoleHasType[] $roleHasTypes
 */
class _Role extends \yii\db\ActiveRecord {
    /**
     * {@inheritdoc}
     */
    public static function tableName(){
        return 'role';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(){
        return [
            [['description'], 'string'],
            [['name'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(){
        return [
            'id'          => 'ID',
            'name'        => 'Name',
            'description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdmins(){
        return $this->hasMany(Admin::className(), ['role_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenus(){
        return $this->hasMany(Menu::className(), ['role_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoleHasCategories(){
        return $this->hasMany(RoleHasCategory::className(), ['role_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoleHasTypes(){
        return $this->hasMany(RoleHasType::className(), ['role_id' => 'id']);
    }
}

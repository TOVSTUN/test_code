<?php

namespace common\entities;

use Yii;

/**
 * This is the model class for table "role_has_type".
 *
 * @property int $role_id
 * @property int $type_id
 *
 * @property Role $role
 * @property Type $type
 */
class RoleHasType extends \yii\db\ActiveRecord {
    /**
     * {@inheritdoc}
     */
    public static function tableName(){
        return 'role_has_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(){
        return [
            [['role_id', 'type_id'], 'required'],
            [['role_id', 'type_id'], 'integer'],
            [['role_id'], 'exist', 'skipOnError' => true, 'targetClass' => Role::className(), 'targetAttribute' => ['role_id' => 'id']],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => Type::className(), 'targetAttribute' => ['type_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(){
        return [
            'role_id' => 'Role ID',
            'type_id' => 'Type ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRole(){
        return $this->hasOne(Role::className(), ['id' => 'role_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType(){
        return $this->hasOne(Type::className(), ['id' => 'type_id']);
    }
}

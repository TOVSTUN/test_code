<?php


namespace common\entities;


use common\traits\Filters;
use yii\helpers\ArrayHelper;

class Period extends _Period {
    use Filters;

    public static function getAllPeriods(){
        return ArrayHelper::map(self::find()->all(), 'id', 'name');
    }

}
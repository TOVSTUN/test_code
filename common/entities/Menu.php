<?php
/**
 * Created by PhpStorm.
 * User: tovstun
 * Date: 08.03.19
 * Time: 14:23
 */

namespace common\entities;

use common\traits\MultiLanguage;

class Menu extends _Menu {

    use MultiLanguage;
}
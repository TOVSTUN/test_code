<?php


namespace common\entities;


use common\helpers\MainHelper;
use Yii;

class Document extends _Document {

    public $imageFile;


    public function rules(){
        return [
            [['date_start', 'date_end', 'time_payment', 'time_delivery', 'company_id', 'category_id', 'type_id', 'period_id', 'admin_id', 'date_init'], 'integer'],
            [['files', 'description', 'date_init_str', 'date_start_str', 'date_end_str'], 'string'],
            [['company_id'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['number'], 'string', 'max' => 100],
            [['admin_id'], 'exist', 'skipOnError' => true, 'targetClass' => Admin::className(), 'targetAttribute' => ['admin_id' => 'id']],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['period_id'], 'exist', 'skipOnError' => true, 'targetClass' => Period::className(), 'targetAttribute' => ['period_id' => 'id']],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => Type::className(), 'targetAttribute' => ['type_id' => 'id']],
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'xlsx, doc, docx, pdf', 'maxFiles' => 10, 'on' => 'fileUploaded'],
            [['imageFile'], 'safe'],
        ];
    }


    public function beforeSave($insert){
        $this->date_init = MainHelper::decodeTime_Y_M_D($this->date_init_str);
        $this->date_start = MainHelper::decodeTime_Y_M_D($this->date_start_str);
        $this->date_end = MainHelper::decodeTime_Y_M_D($this->date_end_str);


        return parent::beforeSave($insert);
    }

    public function upload(){

        foreach($this->imageFile as $file){

            $newName = Yii::$app->security->generateRandomString();
            $newFileName = $newName . '.' . $file->extension;
            //   print_r($file->size); exit;

            if($file->saveAs('uploads/' . $newFileName)){
                $myFile = new File();
                $myFile->company_id = $this->company_id;
                $myFile->document_id = $this->id;
                $myFile->hash = $newFileName;
                $myFile->name = $file->baseName;
                $myFile->extension = '.' . $file->extension;
                $myFile->size = $file->size;
                $myFile->save();
            }
        }
        return true;
    }


}
<?php

namespace common\entities;

use Yii;

/**
 * This is the model class for table "file".
 *
 * @property int $id
 * @property string $name оригінальна назва документу
 * @property int $is_main маркер головного документу
 * @property string $hash назва файла в сховку файлів
 * @property int $company_id
 * @property int $document_id
 * @property string $extension
 * @property int $size
 *
 * @property Company $company
 * @property Document $document
 */
class _File extends \yii\db\ActiveRecord {
    /**
     * {@inheritdoc}
     */
    public static function tableName(){
        return 'file';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(){
        return [
            [['is_main', 'company_id', 'document_id', 'size'], 'integer'],
            [['company_id', 'document_id'], 'required'],
            [['name', 'hash', 'extension'], 'string', 'max' => 255],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['document_id'], 'exist', 'skipOnError' => true, 'targetClass' => Document::className(), 'targetAttribute' => ['document_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(){
        return [
            'id'          => 'ID',
            'name'        => 'Name',
            'is_main'     => 'Is Main',
            'hash'        => 'Hash',
            'company_id'  => 'Company ID',
            'document_id' => 'Document ID',
            'extension'   => 'Extension',
            'size'        => 'Size',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany(){
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocument(){
        return $this->hasOne(Document::className(), ['id' => 'document_id']);
    }
}

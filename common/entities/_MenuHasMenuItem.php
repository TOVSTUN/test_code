<?php

namespace common\entities;

use Yii;

/**
 * This is the model class for table "menu_has_menu_item".
 *
 * @property int $menu_id
 * @property int $menu_item_id
 *
 * @property Menu $menu
 * @property MenuItem $menuItem
 */
class _MenuHasMenuItem extends \yii\db\ActiveRecord {
    /**
     * {@inheritdoc}
     */
    public static function tableName(){
        return 'menu_has_menu_item';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(){
        return [
            [['menu_id', 'menu_item_id'], 'required'],
            [['menu_id', 'menu_item_id'], 'integer'],
            [['menu_id', 'menu_item_id'], 'unique', 'targetAttribute' => ['menu_id', 'menu_item_id']],
            [['menu_id'], 'exist', 'skipOnError' => true, 'targetClass' => Menu::className(), 'targetAttribute' => ['menu_id' => 'id']],
            [['menu_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => MenuItem::className(), 'targetAttribute' => ['menu_item_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(){
        return [
            'menu_id'      => 'Menu ID',
            'menu_item_id' => 'Menu Item ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenu(){
        return $this->hasOne(Menu::className(), ['id' => 'menu_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuItem(){
        return $this->hasOne(MenuItem::className(), ['id' => 'menu_item_id']);
    }
}

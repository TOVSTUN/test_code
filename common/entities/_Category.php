<?php

namespace common\entities;

use Yii;

/**
 * This is the model class for table "category".
 *
 * @property int $id
 * @property string $name
 *
 * @property AdminHasCategory[] $adminHasCategories
 * @property Document[] $documents
 * @property RoleHasCategory[] $roleHasCategories
 */
class _Category extends \yii\db\ActiveRecord {
    /**
     * {@inheritdoc}
     */
    public static function tableName(){
        return 'category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(){
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(){
        return [
            'id'   => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdminHasCategories(){
        return $this->hasMany(AdminHasCategory::className(), ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocuments(){
        return $this->hasMany(Document::className(), ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoleHasCategories(){
        return $this->hasMany(RoleHasCategory::className(), ['categoty_id' => 'id']);
    }
}

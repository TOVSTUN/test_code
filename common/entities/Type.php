<?php


namespace common\entities;


use common\traits\Filters;

class Type extends _Type {
    const IS_SENDER = 3;
    const IS_DESTINATION = 1;

    use Filters;
}
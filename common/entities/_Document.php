<?php

namespace common\entities;

use Yii;

/**
 * This is the model class for table "document".
 *
 * @property int $id
 * @property int $date_start початок договору
 * @property int $date_end кінець договору
 * @property int $time_payment артефакт - на видалення
 * @property int $time_delivery артефакт - на видалення
 * @property string $files артефакт - на видалення
 * @property string $description опис договору
 * @property int $company_id
 * @property string $name назва договору
 * @property string $number номер договору
 * @property int $category_id
 * @property int $type_id
 * @property int $period_id okres wypowiedzenia
 * @property int $admin_id opiekun umowy
 * @property int $date_init
 * @property string $date_init_str
 * @property string $date_start_str
 * @property string $date_end_str
 * @property int $status Статус який сигналізує який стан цієї умови закрита або відкрита
 * @property int $created_admin_id
 *
 * @property AdminLog[] $adminLogs
 * @property Admin $createdAdmin
 * @property Admin $admin
 * @property Category $category
 * @property Company $company
 * @property Period $period
 * @property Type $type
 * @property File[] $files0
 */
class _Document extends \yii\db\ActiveRecord {
    /**
     * {@inheritdoc}
     */
    public static function tableName(){
        return 'document';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(){
        return [
            [['date_start', 'date_end', 'time_payment', 'time_delivery', 'company_id', 'category_id', 'type_id', 'period_id', 'admin_id', 'date_init', 'status', 'created_admin_id'], 'integer'],
            [['files', 'description'], 'string'],
            [['company_id'], 'required'],
            [['name', 'date_init_str', 'date_start_str', 'date_end_str'], 'string', 'max' => 255],
            [['number'], 'string', 'max' => 100],
            [['created_admin_id'], 'exist', 'skipOnError' => true, 'targetClass' => Admin::className(), 'targetAttribute' => ['created_admin_id' => 'id']],
            [['admin_id'], 'exist', 'skipOnError' => true, 'targetClass' => Admin::className(), 'targetAttribute' => ['admin_id' => 'id']],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['period_id'], 'exist', 'skipOnError' => true, 'targetClass' => Period::className(), 'targetAttribute' => ['period_id' => 'id']],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => Type::className(), 'targetAttribute' => ['type_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(){
        return [
            'id'               => 'ID',
            'date_start'       => 'Date Start',
            'date_end'         => 'Date End',
            'time_payment'     => 'Time Payment',
            'time_delivery'    => 'Time Delivery',
            'files'            => 'Files',
            'description'      => 'Description',
            'company_id'       => 'Company ID',
            'name'             => 'Name',
            'number'           => 'Number',
            'category_id'      => 'Category ID',
            'type_id'          => 'Type ID',
            'period_id'        => 'Period ID',
            'admin_id'         => 'Admin ID',
            'date_init'        => 'Date Init',
            'date_init_str'    => 'Date Init Str',
            'date_start_str'   => 'Date Start Str',
            'date_end_str'     => 'Date End Str',
            'status'           => 'Status',
            'created_admin_id' => 'Created Admin ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdminLogs(){
        return $this->hasMany(AdminLog::className(), ['document_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedAdmin(){
        return $this->hasOne(Admin::className(), ['id' => 'created_admin_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdmin(){
        return $this->hasOne(Admin::className(), ['id' => 'admin_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory(){
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany(){
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriod(){
        return $this->hasOne(Period::className(), ['id' => 'period_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType(){
        return $this->hasOne(Type::className(), ['id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFiles0(){
        return $this->hasMany(File::className(), ['document_id' => 'id']);
    }
}

<?php

namespace common\entities;

use Yii;

/**
 * This is the model class for table "menu_item".
 *
 * @property int $id
 * @property string $url урл сторінки куди вказує цей пункт
 * @property int $status статус - активек 10 / неактивне 0\n
 * @property string $icon код іконки зі ширифта авеслоле - іконка меню
 * @property string $ru_name
 * @property string $ua_name
 * @property string $pl_name
 * @property string $en_name
 * @property string $ru_description
 * @property string $ua_description
 * @property string $pl_description
 * @property string $en_description
 * @property int $global 10/0  маркер який вказує приналежність цього елементу як що 10 це загальний елемент який використовується у різних меню 0 - це локальний елемент для одного вибраного меню
 *
 * @property MenuHasMenuItem[] $menuHasMenuItems
 * @property Menu[] $menus
 */
class _MenuItem extends \yii\db\ActiveRecord {
    /**
     * {@inheritdoc}
     */
    public static function tableName(){
        return 'menu_item';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(){
        return [
            [['status', 'global'], 'integer'],
            [['url'], 'string', 'max' => 255],
            [['icon', 'ru_name', 'ua_name', 'pl_name', 'en_name'], 'string', 'max' => 45],
            [['ru_description', 'ua_description', 'pl_description', 'en_description'], 'string', 'max' => 1000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(){
        return [
            'id'             => 'ID',
            'url'            => 'Url',
            'status'         => 'Status',
            'icon'           => 'Icon',
            'ru_name'        => 'Ru Name',
            'ua_name'        => 'Ua Name',
            'pl_name'        => 'Pl Name',
            'en_name'        => 'En Name',
            'ru_description' => 'Ru Description',
            'ua_description' => 'Ua Description',
            'pl_description' => 'Pl Description',
            'en_description' => 'En Description',
            'global'         => 'global',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuHasMenuItems(){
        return $this->hasMany(MenuHasMenuItem::className(), ['menu_item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenus(){
        return $this->hasMany(Menu::className(), ['id' => 'menu_id'])->viaTable('menu_has_menu_item', ['menu_item_id' => 'id']);
    }
}

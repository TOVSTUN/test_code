<?php

namespace common\entities;

use Yii;

/**
 * This is the model class for table "role_has_category".
 *
 * @property int $role_id
 * @property int $categoty_id
 *
 * @property Role $role
 * @property Category $categoty
 */
class RoleHasCategory extends \yii\db\ActiveRecord {
    /**
     * {@inheritdoc}
     */
    public static function tableName(){
        return 'role_has_category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(){
        return [
            [['role_id', 'categoty_id'], 'required'],
            [['role_id', 'categoty_id'], 'integer'],
            [['role_id'], 'exist', 'skipOnError' => true, 'targetClass' => Role::className(), 'targetAttribute' => ['role_id' => 'id']],
            [['categoty_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['categoty_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(){
        return [
            'role_id'     => 'Role ID',
            'categoty_id' => 'Categoty ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRole(){
        return $this->hasOne(Role::className(), ['id' => 'role_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoty(){
        return $this->hasOne(Category::className(), ['id' => 'categoty_id']);
    }
}

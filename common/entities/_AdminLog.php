<?php

namespace common\entities;

use Yii;

/**
 * This is the model class for table "admin_log".
 *
 * @property int $id
 * @property int $created_at
 * @property int $admin_id
 * @property int $document_id
 *
 * @property Document $document
 * @property Admin $admin
 */
class _AdminLog extends \yii\db\ActiveRecord {
    /**
     * {@inheritdoc}
     */
    public static function tableName(){
        return 'admin_log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(){
        return [
            [['created_at', 'admin_id', 'document_id'], 'required'],
            [['created_at', 'admin_id', 'document_id'], 'integer'],
            [['document_id'], 'exist', 'skipOnError' => true, 'targetClass' => Document::className(), 'targetAttribute' => ['document_id' => 'id']],
            [['admin_id'], 'exist', 'skipOnError' => true, 'targetClass' => Admin::className(), 'targetAttribute' => ['admin_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(){
        return [
            'id'          => 'ID',
            'created_at'  => 'Created At',
            'admin_id'    => 'Admin ID',
            'document_id' => 'Document ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocument(){
        return $this->hasOne(Document::className(), ['id' => 'document_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdmin(){
        return $this->hasOne(Admin::className(), ['id' => 'admin_id']);
    }
}

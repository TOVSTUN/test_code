<?php

namespace common\entities;

use Yii;

/**
 * This is the model class for table "admin_has_type".
 *
 * @property int $admin_id
 * @property int $type_id
 *
 * @property Type $type
 * @property Admin $admin
 */
class AdminHasType extends \yii\db\ActiveRecord {
    /**
     * {@inheritdoc}
     */
    public static function tableName(){
        return 'admin_has_type';
    }

    public static function primaryKey(){
        return [
            'type_id',
            'admin_id',
        ];
    }

    public function rules(){
        return [
            [['admin_id', 'type_id'], 'required'],
            [['admin_id', 'type_id'], 'integer'],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => Type::className(), 'targetAttribute' => ['type_id' => 'id']],
            [['admin_id'], 'exist', 'skipOnError' => true, 'targetClass' => Admin::className(), 'targetAttribute' => ['admin_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(){
        return [
            'admin_id' => 'Admin ID',
            'type_id'  => 'Type ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType(){
        return $this->hasOne(Type::className(), ['id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdmin(){
        return $this->hasOne(Admin::className(), ['id' => 'admin_id']);
    }
}

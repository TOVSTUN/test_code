<?php

namespace common\entities;

use Yii;

/**
 * This is the model class for table "admin".
 *
 * @property int $id
 * @property string $username
 * @property string $auth_key ключ авторизації
 * @property string $email
 * @property string $password_hash
 * @property string $password_reset_token
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 * @property string $fast_menu серіалізовані idiшки пунктів меню для меню швидкого доступу
 * @property int $last_visit час останнього логування в системі
 * @property int $current_visit час теперішнього логування в системі
 * @property resource $avatar
 * @property int $role_id
 *
 * @property Role $role
 * @property AdminHasCategory[] $adminHasCategories
 * @property AdminHasType[] $adminHasTypes
 * @property AdminLog[] $adminLogs
 * @property Document[] $documents
 * @property Document[] $documents0
 */
class _Admin extends \yii\db\ActiveRecord {
    /**
     * {@inheritdoc}
     */
    public static function tableName(){
        return 'admin';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(){
        return [
            [['username', 'auth_key', 'email', 'password_hash', 'status'], 'required'],
            [['status', 'created_at', 'updated_at', 'last_visit', 'current_visit', 'role_id'], 'integer'],
            [['fast_menu', 'avatar'], 'string'],
            [['username', 'email'], 'string', 'max' => 45],
            [['auth_key', 'password_hash', 'password_reset_token'], 'string', 'max' => 255],
            [['email'], 'unique'],
            [['role_id'], 'exist', 'skipOnError' => true, 'targetClass' => Role::className(), 'targetAttribute' => ['role_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(){
        return [
            'id'                   => 'ID',
            'username'             => 'Username',
            'auth_key'             => 'Auth Key',
            'email'                => 'Email',
            'password_hash'        => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'status'               => 'Status',
            'created_at'           => 'Created At',
            'updated_at'           => 'Updated At',
            'fast_menu'            => 'Fast Menu',
            'last_visit'           => 'Last Visit',
            'current_visit'        => 'Current Visit',
            'avatar'               => 'Avatar',
            'role_id'              => 'Role ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRole(){
        return $this->hasOne(Role::className(), ['id' => 'role_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdminHasCategories(){
        return $this->hasMany(AdminHasCategory::className(), ['admin_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdminHasTypes(){
        return $this->hasMany(AdminHasType::className(), ['admin_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdminLogs(){
        return $this->hasMany(AdminLog::className(), ['admin_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocuments(){
        return $this->hasMany(Document::className(), ['created_admin_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocuments0(){
        return $this->hasMany(Document::className(), ['admin_id' => 'id']);
    }
}

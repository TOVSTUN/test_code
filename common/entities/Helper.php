<?php


namespace common\entities;


use common\helpers\MainHelper;
use common\statuses\MyStatus;

class Helper extends _Helper {


    public static function generateNumber(){

        $helper = self::findOne(1);
        if($helper->year != date('Y') * 1){
            $helper->year = date('Y') * 1;
            $helper->doc_num = 1;
            $helper->num_comfirm = MyStatus::STATUS_INACTIVE;
            $helper->save();
        }

        if($helper->num_comfirm == MyStatus::STATUS_ACTIVE){
            $helper->doc_num = $helper->doc_num + 1;
            $helper->num_comfirm = MyStatus::STATUS_INACTIVE;
            $helper->save();
        }

        return $helper->year . '/' . $helper->doc_num;
    }

    public static function confirmNumber(){
        $helper = self::findOne(1);
        $helper->num_comfirm = MyStatus::STATUS_ACTIVE;
        $helper->save();
    }
}
<?php

namespace common\entities;

use Yii;

/**
 * This is the model class for table "company".
 *
 * @property int $id
 * @property string $acronim
 * @property string $name
 * @property string $adress
 * @property string $description
 * @property int $created_at
 * @property int $updated_at
 * @property string $contact_info телефони вайбери та мило
 * @property int $nip ніп компанії
 *
 * @property AdminHasCompany[] $adminHasCompanies
 * @property Document[] $documents
 * @property File[] $files
 */
class _Company extends \yii\db\ActiveRecord {
    /**
     * {@inheritdoc}
     */
    public static function tableName(){
        return 'company';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(){
        return [
            [['description'], 'string'],
            [['created_at', 'updated_at', 'nip'], 'integer'],
            [['acronim', 'name'], 'string', 'max' => 255],
            [['adress', 'contact_info'], 'string', 'max' => 2000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(){
        return [
            'id'           => 'ID',
            'acronim'      => 'Acronim',
            'name'         => 'Name',
            'adress'       => 'Adress',
            'description'  => 'Description',
            'created_at'   => 'Created At',
            'updated_at'   => 'Updated At',
            'contact_info' => 'Contact Info',
            'nip'          => 'Nip',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdminHasCompanies(){
        return $this->hasMany(AdminHasCompany::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocuments(){
        return $this->hasMany(Document::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFiles(){
        return $this->hasMany(File::className(), ['company_id' => 'id']);
    }
}

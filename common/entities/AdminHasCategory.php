<?php

namespace common\entities;

use Yii;

/**
 * This is the model class for table "admin_has_category".
 *
 * @property int $admin_id
 * @property int $category_id
 *
 * @property Category $category
 * @property Admin $admin
 */
class AdminHasCategory extends \yii\db\ActiveRecord {
    /**
     * {@inheritdoc}
     */
    public static function tableName(){
        return 'admin_has_category';
    }

    public static function primaryKey(){
        return [
            'category_id',
            'admin_id',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules(){
        return [
            [['admin_id', 'category_id'], 'required'],
            [['admin_id', 'category_id'], 'integer'],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['admin_id'], 'exist', 'skipOnError' => true, 'targetClass' => Admin::className(), 'targetAttribute' => ['admin_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(){
        return [
            'admin_id'    => 'Admin ID',
            'category_id' => 'Category ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory(){
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdmin(){
        return $this->hasOne(Admin::className(), ['id' => 'admin_id']);
    }
}

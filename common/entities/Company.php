<?php


namespace common\entities;


use common\traits\Filters;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

class Company extends _Company {
    use Filters;

    public $company_id2;

    public function behaviors(){
        return [
            [
                'class'      => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }

    public static function getAllAcronims(){
        return ArrayHelper::map(self::find()->all(), 'id', 'acronim');

    }
}
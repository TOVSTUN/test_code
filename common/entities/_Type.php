<?php

namespace common\entities;

use Yii;

/**
 * This is the model class for table "type".
 *
 * @property int $id
 * @property string $name
 *
 * @property AdminHasType[] $adminHasTypes
 * @property Document[] $documents
 * @property RoleHasType[] $roleHasTypes
 */
class _Type extends \yii\db\ActiveRecord {
    /**
     * {@inheritdoc}
     */
    public static function tableName(){
        return 'type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(){
        return [
            [['name'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(){
        return [
            'id'   => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdminHasTypes(){
        return $this->hasMany(AdminHasType::className(), ['type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocuments(){
        return $this->hasMany(Document::className(), ['type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoleHasTypes(){
        return $this->hasMany(RoleHasType::className(), ['type_id' => 'id']);
    }
}

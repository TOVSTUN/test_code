<?php

namespace common\entities;

use Yii;

/**
 * This is the model class for table "helper".
 *
 * @property int $id
 * @property int $year
 * @property int $doc_num
 * @property int $num_comfirm
 */
class _Helper extends \yii\db\ActiveRecord {
    /**
     * {@inheritdoc}
     */
    public static function tableName(){
        return 'helper';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(){
        return [
            [['year', 'doc_num', 'num_comfirm'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(){
        return [
            'id'          => 'ID',
            'year'        => 'Year',
            'doc_num'     => 'Doc Num',
            'num_comfirm' => 'Num Comfirm',
        ];
    }
}

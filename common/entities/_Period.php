<?php

namespace common\entities;

use Yii;

/**
 * This is the model class for table "period".
 *
 * @property int $id
 * @property string $name
 *
 * @property Document[] $documents
 */
class _Period extends \yii\db\ActiveRecord {
    /**
     * {@inheritdoc}
     */
    public static function tableName(){
        return 'period';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(){
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(){
        return [
            'id'   => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocuments(){
        return $this->hasMany(Document::className(), ['period_id' => 'id']);
    }
}

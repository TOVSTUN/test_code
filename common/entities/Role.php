<?php


namespace common\entities;


use yii\helpers\ArrayHelper;

class Role extends _Role {

    public static function getAllRoles(){
        return ArrayHelper::map(self::find()->all(), 'id', 'name');
    }

}
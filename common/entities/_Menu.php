<?php

namespace common\entities;

use Yii;

/**
 * This is the model class for table "menu".
 *
 * @property int $id
 * @property string $system_name назва значення цьогоменю оприділена в класі констрант Довільна назва меню, щоб можна було його знайти для вставки в кожд за цією назвою для виклику його у віджеті генератора меню як приклад тут стоїть імя константи цього меню
 * @property int $status 0 неактивно 10 автивно
 * @property int $order сортування,  номер елемента
 * @property int $name_visibility маркер який вказує- виводит назву меню чі ні 0 ні / 10 так
 * @property string $items_string json масив для виводу id пунктів меню з таблиці  menu_item
 * @property int $item_name_visibility 10/0 -  показувати назву пункту меню
 * @property int $item_icon_visibility 10 - так / 0- ні
 * @property string $old_items_string резервна копія меню,  саме меню часто злітає підстраховуємося копією ))
 * @property int $old_items_string_update дата створення резервної копії меню
 * @property int $role_id до вияснення остаточної версії сбди підставляємо значення з константи яку оприділяємо в класі юзера (admin 1 lj 100) визначытися що сюди вписуэмо, може доцыльныше  буде сконфыгурувати його через  ENUM
 * @property string $ru_name
 * @property string $ua_name
 * @property string $pl_name
 * @property string $en_name
 * @property string $ru_description
 * @property string $ua_description
 * @property string $pl_description
 * @property string $en_description
 *
 * @property Role $role
 * @property MenuHasMenuItem[] $menuHasMenuItems
 * @property MenuItem[] $menuItems
 */
class _Menu extends \yii\db\ActiveRecord {
    /**
     * {@inheritdoc}
     */
    public static function tableName(){
        return 'menu';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(){
        return [
            [['system_name', 'item_icon_visibility', 'role_id'], 'required'],
            [['status', 'order', 'name_visibility', 'item_name_visibility', 'item_icon_visibility', 'old_items_string_update', 'role_id'], 'integer'],
            [['items_string', 'old_items_string'], 'string'],
            [['system_name', 'ru_name', 'ua_name', 'pl_name', 'en_name'], 'string', 'max' => 45],
            [['ru_description', 'ua_description', 'pl_description', 'en_description'], 'string', 'max' => 1000],
            [['role_id'], 'exist', 'skipOnError' => true, 'targetClass' => Role::className(), 'targetAttribute' => ['role_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(){
        return [
            'id'                      => 'ID',
            'system_name'             => 'System Name',
            'status'                  => 'Status',
            'order'                   => 'Order',
            'name_visibility'         => 'Name Visibility',
            'items_string'            => 'Items String',
            'item_name_visibility'    => 'Item Name Visibility',
            'item_icon_visibility'    => 'Item Icon Visibility',
            'old_items_string'        => 'Old Items String',
            'old_items_string_update' => 'Old Items String Update',
            'role_id'                 => 'Role ID',
            'ru_name'                 => 'Ru Name',
            'ua_name'                 => 'Ua Name',
            'pl_name'                 => 'Pl Name',
            'en_name'                 => 'En Name',
            'ru_description'          => 'Ru Description',
            'ua_description'          => 'Ua Description',
            'pl_description'          => 'Pl Description',
            'en_description'          => 'En Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRole(){
        return $this->hasOne(Role::className(), ['id' => 'role_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuHasMenuItems(){
        return $this->hasMany(MenuHasMenuItem::className(), ['menu_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuItems(){
        return $this->hasMany(MenuItem::className(), ['id' => 'menu_item_id'])->viaTable('menu_has_menu_item', ['menu_id' => 'id']);
    }
}

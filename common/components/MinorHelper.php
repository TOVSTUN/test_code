<?php
/**
 * Created by PhpStorm.
 * User: tovstun
 * Date: 28.03.19
 * Time: 21:06
 */

namespace common\components;


use common\entities\Admin;
use common\entities\Role;
use common\entities\User;
use Yii;

class MinorHelper {

    /**Поверне масив всіх ролей які використовуються в системі Адміністраторів та звичайних юзерів
     * @return array
     */
    public static function getAllUsersRole(){
        return Role::getAllRoles();
    }

    public static function getUserRole($role_id){

        $a = self::getAllUsersRole();
        return $a[$role_id];
    }

    /**Помічник який приймає число з неправильним делімітером та замінює/форматує йогго у форматі 0.00 його на крапку,
     * та зашишає після символу 2 знаки
     * @param $data
     * @return mixed
     */
    public static function correctNumbers($data = null){

        if($data){
            $data = round(str_replace(',', '.', $data), 2);
            // return number_format((float)$data, 2, '.', '');
            return $data;
        }
        return null;
    }

    public static function moneyDisplay($data){

        return number_format($data, 2, ',', ' ') . ' zł';

    }

    public static function checkTestHost(){
        if(Yii::$app->db->dsn == 'mysql:host=192.168.142.11;dbname=umowy'){
            echo '<div style="background-color: #27ff00; height: 5px"></div>';
        }

    }

}
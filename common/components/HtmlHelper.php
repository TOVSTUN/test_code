<?php
/**
 * Created by PhpStorm.
 * User: tovstun
 * Date: 07.06.19
 * Time: 11:08
 */

namespace common\components;


class HtmlHelper {

    public static function getOffIcon(){
        return '<i class="fa fa-toggle-off toggle-icon  color-red"></i>';
    }

    public static function getOnIcon(){
        return '<i class="fa fa-toggle-on toggle-icon  color-green"></i>';
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: tovstun
 * Date: 25.05.17
 * Time: 17:56
 */

namespace common\components;

/**Конфігурація глобальних ролей доступу  використовуємо в поведінках behaviors() в контролерах
 * public function behaviors()
 * {
 * return [
 * 'access' => MyAccessRule::globalRule([
 * Admin::IS_BOSS,
 * Admin::,
 * ]),
 * 'verbs' => [
 * 'class' => VerbFilter::className(),
 * 'actions' => [
 * 'delete' => ['POST'],
 * ],
 * ],
 * ];
 * }
 */

use core\entities\Admin;
use Yii;
use yii\filters\AccessRule;

class MyAccessRule extends AccessRule {
    protected function matchRole($user){
        if(empty($this->roles)){
            return true;
        }
        foreach($this->roles as $role){

            if(!$user->getIsGuest() && $role === $user->identity->role_id) return true;
            if(!$user->getIsGuest() && $role === '@' && $user->identity->role_id > 0) return true;
            if($user->getIsGuest() && $role === '?') return true;
        }

        return false;
    }

    /**Конфігурація глобальних ролей доступу  використовуємо в поведінках behaviors() в контролерах
     *      'access' => MyAccessRule::globalRule([
     * Admin::IS_BOSS,
     * Admin::IS_ROOT,
     * ]),
     * передаємо тільки ті ролі юзерів які мають доступ до цього контролера або модуля (як що конфігуружмо модулі)
     *
     * або так  'access' => MyAccessRule::globalRule(['@']), -> доступ тільки для авторизованих юзерів
     * @param $roles
     * @return array
     */
    public static function globalRule($roles){
        return [
            'class'        => 'yii\filters\AccessControl',
            'denyCallback' => function($rule, $action){
                //Yii::$app->user->logout();
                Yii::$app->session->setFlash('success', 'Przykro nam, ale nie masz uprawnień dostępu do tej informacji');
                return Yii::$app->getResponse()->redirect('/dashboard/start');
            },
            'ruleConfig'   => [
                'class' => MyAccessRule::className(),
            ],
            'rules'        => [
                [
                    'allow' => true,
                    'roles' => $roles,
                ],
            ],
        ];

    }

    /**Конфігурація локальних ролей доступу наприклад до action
     * MyAccessRule::localRule([
     * 'roles' => [
     * Admin::IS_BOSS,
     * Admin::IS_ROOT
     * ],
     * 'redirect' => '/lite-order-item/' . $order_id,
     * ]);
     * передаємо два парметри ролі доступу та потрібний редирект при потребі, (по дефолту редиректне на головну сторінку)
     * @param $info
     * @return bool|\yii\console\Response|\yii\web\Response
     */
    public static function localRule($info){

        $info = (object)$info;
        $role_id = Yii::$app->user->identity->user_role_id;

        if(!in_array($role_id, $info->roles)){
            $redirect = $info->redirect ?? '/site/not-active';
            return Yii::$app->response->redirect([$redirect]);
        }
    }
}
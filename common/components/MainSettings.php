<?php
/**
 * Created by PhpStorm.
 * User: tovstun
 * Date: 25.03.19
 * Time: 8:53
 */

namespace common\components;


use yii\base\Component;

/**Головний конфігураціний файл системи, тут тримаємо всі конфігураціний параметри сервісу
 * Class MainSettings
 * @package common\components
 */
class MainSettings extends Component {

    /** Вказуємо тип мовних налаштувань сайту
     * true - мультиязичний сайт
     * false - одноязичний
     * @var
     */
    public $single_language = true;

    /** Вказуємо в ключовому форматі (ru, pl, ua) мову позамовчуванню, це потрібно для підстановки та витягування правильної локалізації з меню
     * @var
     */
    public $default_language;

    /** генеруємо сюди з кук або сесії у  форматі (ru, pl, ua) мову сторінки,
     * @var
     */
    public $user_language; //S0 БЕЗПЕКА Обовязково зроби тут перевірку по масці дозволених ключів мов, і тільки тоді підставляй це сюди
}
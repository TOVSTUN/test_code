<?php
/**
 * Created by PhpStorm.
 * User: tovstun
 * Date: 23/07/19
 * Time: 10:39
 */

namespace common\traits;

use yii\helpers\ArrayHelper;

/**
 * колекція різних типових виборок та фільтрів для обєктів Entities
 */
trait Filters {

    /**
     * цей метод поверне простий масив у форматі ['id' => 'name'] для обєкта entities
     * має отримати $company_id
     */
    public static function getSimpleFilter(): array{
        $a = static::find()
            //->where(['company_id' => $company_id])
            ->andWhere(['not', ['name' => null]])
            ->orderBy(['name' => SORT_ASC])
            ->asArray()
            ->all();
        return is_array($a) ? ArrayHelper::map($a, 'id', 'name') : [];
    }
}
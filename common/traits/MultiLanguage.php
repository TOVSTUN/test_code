<?php
/**
 * Created by PhpStorm.
 * User: tovstun
 * Date: 30.03.19
 * Time: 8:03
 */

namespace common\traits;

trait MultiLanguage {
    public $name;
    public $description;

    private $language_key;


    public function init(){
        parent::init();

        $this->language_key = \Yii::$app->settings->user_language;
        if(!$this->language_key) $this->language_key = \Yii::$app->settings->default_language;

    }

    public function rules(){
        $rules = [[['name', 'description'], 'string']];
        return array_merge(parent::rules(), $rules);
    }

    public function afterFind(){

        if($this->language_key != 'all'){ //S0 незрозумілий механізм all, теоретично це порібно для транслятора, але якось це все ненативно працює
            $this->name = $this->{$this->language_key . '_name'};
            $this->description = $this->{$this->language_key . '_description'};
        }
        parent::afterFind();

    }

    public function beforeSave($insert){

        if($this->name) $this->{$this->language_key . '_name'} = $this->name;
        if($this->description) $this->{$this->language_key . '_description'} = $this->description;
        return parent::beforeSave($insert);
    }

}
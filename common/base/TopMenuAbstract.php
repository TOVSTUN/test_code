<?php
/**
 * Created by PhpStorm.
 * User: tovstun
 * Date: 05.04.19
 * Time: 11:45
 */

namespace common\base;

use Yii;

abstract class TopMenuAbstract {

    /**
     * @param $obj - це обєкт $this з вигляду. В цьому методі ми корнфігуруємо верхне міні меню, частіше всього використовужмо для адмінки
     * конфігурація проста
     * $path = [
     * '/company'                                => 'Wszystkie firmy',
     * '/company/info/' . $company->id           => 'O firmie',
     * '/company/building/index/' . $company->id => 'Budynki',
     * ];
     * parent::generateBreadctumbs($obj, $path);
     * сконфігурвоаний patch передай обов'язково в generateBreadctumbs
     * в стилях style2/.my-breadcrumb задаємо параметри класу
     */
    abstract public static function showBreadctumbs($obj, $company = null);

    /**
     * це генератор Breadctumbs
     */
    protected static function generateBreadctumbs($obj, $path){
        foreach($path as $url => $name){
            //  print_r($url);exit();
            $class = ('/' . Yii::$app->request->pathInfo == $url) ? 'my-breadcrumb' : true;
            $obj->params['breadcrumbs'][] = ['label' => $name, 'url' => [$url], 'class' => $class];
        }
    }
}
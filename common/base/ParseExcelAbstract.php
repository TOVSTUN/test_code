<?php


namespace common\base;


use Yii;

abstract class ParseExcelAbstract {

    abstract static function run($file);

    protected static function getNumColumn($aliases, $file){

        foreach($aliases as $value){
            if(in_array($value, $file->getLegend())){
                return array_search($value, $file->getLegend());
            }
        }
        Yii::$app->session->setFlash('danger', 'BŁAD! - nieuprawniona modyfikacja nazwy kolumny <strong>' . $aliases[0] . '</strong>, A-TA-TA, Nie wolno tak robić');

        Yii::$app->response->redirect(Yii::$app->request->referrer)->send();
        exit();
    }

}
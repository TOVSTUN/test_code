<?php
/**
 * Created by PhpStorm.
 * User: tovstun
 * Date: 11.12.18
 * Time: 13:56
 */

namespace common\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;


class FileExcelModel extends Model {
    /**
     * @var UploadedFile
     */
    public $excel_file;

    public function rules(){
        return [
            [['excel_file'], 'file', 'skipOnEmpty' => true, 'extensions' => ['xls', 'xlsx', 'xlsm'], 'checkExtensionByMimeType' => false],
        ];
    }

    public function upload(){
        if($this->validate() && isset($this->excel_file->baseName)){
            $this->excel_file->saveAs(Yii::getAlias('@webroot') . '/uploads/' . $this->excel_file->baseName . '.' . $this->excel_file->extension);
            return true;
        }
        else{
            return false;
        }
    }
}

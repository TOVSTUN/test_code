<?php
/**
 * Created by PhpStorm.
 * User: tovstun
 * Date: 27.08.19
 * Time: 13:25
 */

namespace common\helpers;


use common\entities\Company;
use Yii;

class SecondaryHelper {


    /**Помічник який приймає число з неправильним делімітером та замінює/форматує йогго у форматі 0.00 його на крапку,
     * та зашишає після символу 2 знаки
     * @param $data
     * @return mixed
     */
    public static function correctNumbers($data = null){

        if($data){
            $data = round(str_replace(',', '.', $data), 2);
            return number_format((float)$data, 2, '.', '');
            //return $data;
        }
        return '0.00';
    }

    public static function moneyDisplay($data){

        return number_format($data, 2, ',', ' ') . ' zł';

    }

    /** Помічник який поверне назву бази даних з якою зараз працює система
     */
    public static function getDBName(){
        if(preg_match('/' . 'dbname' . '=([^;]*)/', Yii::$app->getDb()->dsn, $match)){
            return $match[1];
        }
        else{
            return null;
        }
    }

    /** Помічник який поверн true як що ми підключені до тестової бази даних
     */
    public static function isTestDb(){
        return self::getDBName() == 'test' ? true : false;
    }

    /**Метод який сконвертує масив IDішок в строку для SQL запита IN (10,20,30,36,365,566)
     * доповнення для хелпера getColumn  $ids = ArrayHelper::getColumn($array, 'id');
     */
    public static function convertIdsToString($ids){
        if(is_array($ids)){
            $q = '';
            foreach($ids as $id){
                $q .= $id . ',';
            }
        }
        return rtrim($q, ',');
    }

}
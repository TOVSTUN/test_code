<?php

namespace common\helpers;


use yii\helpers\Html;

class DesignHelper {
    /**
     * Цей метод додає модальны кнопки для круда
     *
     * ['class' => 'yii\grid\ActionColumn', 'buttons' => DesignHelper::controlModalboottons()],
     */
    public static function controlModalBoottonsMd(){
        return ['update' => function($url, $model){
            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['class' => 'my-modal-md', 'data-pjax' => '0']);
        }, 'view'        => function($url, $model){
            return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['class' => 'my-modal-md', 'data-pjax' => '0']);
        },];
    }

    /**
     * Цей метод додає модальны кнопки для круда
     *
     * ['class' => 'yii\grid\ActionColumn', 'buttons' => DesignHelper::controlModalboottons()],
     */
    public static function controlModalBoottonsLg(){
        return ['update' => function($url, $model){
            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['class' => 'my-modal-lg', 'data-pjax' => '0']);
        }, 'view'        => function($url, $model){
            return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['class' => 'my-modal-lg', 'data-pjax' => '0']);
        },];
    }

}
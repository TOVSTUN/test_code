<?php
/**
 * Created by PhpStorm.
 * User: tovstun
 * Date: 24.01.18
 * Time: 0:18
 */

namespace common\helpers;


/**Клас для роботи зі строками та json масивами
 * Class StringHelper
 * @package common\helpers
 */
class StringHelper {


    /**Функція яка вичістить ствоку забере всі лишні пробілі та переноси строк тільки в простих текстах, це не буде працювати
     * в складних відформатованих текстах html
     * @param $text
     * @return null|string|string[]
     */
    public static function cleaning($text){
        return preg_replace('/^ +| +$|( ) +/m', '$1', str_replace("\n", " ", $text));
    }

    /**Функція яка з серіалізованного багатомірного json массиву зробить простий масиив
     * [{"id":3,"children":[{"id":5}]},{"id":4}]   ===>  Array ( [0] => 3 [1] => 4 [2] => 5)
     * @param $jsonString
     * @return array
     */
    public static function convertToSimpleArray($jsonString){
        return self::convertToSimpleArrayTmp(json_decode($jsonString));
    }

    private static function convertToSimpleArrayTmp($jsonString, &$info = []){
        if($jsonString){
            foreach($jsonString as $value){

                if(isset($value->children)){
                    $info[] = $value->id;
                    self::convertToSimpleArrayTmp($value->children, $info);
                }
                else $info[] = $value->id;
            }
        }
        return array_unique($info);
    }

    /** Шукає батьків по вказанному параметру айді
     * приймає 2 параметра айді батьків якіх потрібно знати та не сформатований jsonList,
     * третій параметр це маркер який як що він null то не включае в масий айдішку яку шукають
     * повертає масив з батьками
     *
     * @param $search_id
     * @param $jsonList
     * @param bool $marker
     * @return array
     */
    public static function findParents($search_id, $jsonList, $marker = true){

        if($marker) $parents = [$search_id];
        self::parentsEach($search_id, $parents, self::convertToArray($jsonList));
        return array_reverse($parents);
    }

    /**
     * внутрішній клас перебирае масиви працює в методі парентс
     * @param $search_id
     * @param $parents
     * @param null $tree
     * @return bool
     */
    private static function parentsEach($search_id, &$parents, $tree = null){
        foreach($tree as $key => $item){
            if($key == $search_id){
                return true;
            }
            else if($item && self::parentsEach($search_id, $parents, $item)){
                $parents[] = $key;
                return true;
            }
        }
        return false;
    }

    /**
     * Перебудує jsonList на звичайтий масив де ключами виступають айдішки розділів
     *       Array  (
     * [26] => 0
     * [6] => Array
     * (   [19] => 0
     * [7] => Array
     * @param $jsonList
     * @param array $info
     * @return array
     * @internal param $this ->jsonList
     */
    public static function convertToArray($jsonString){
        //return self::convertToArrayTmp(json_decode($jsonString));
        return json_decode($jsonString);

    }

    /*   private static function convertToArrayTmp($jsonList, &$info = array())
       {
           print_r($jsonList); exit();
           //var_dump(); exit();
           foreach ($jsonList as $value) {

               if (isset($value->children)) {
                   //$info[$value->id] = array_column($value->children, 'children', 'id');
                   self::convertToArrayTmp($value->children, $info[$value->id]);
                   $info[] = ['id'=>$value->id, 'children'=>$value->children[0]];
               } else $info[] =['id'=>$value->id];
           }
           return $info;
       }*/

    /** Поверне всіх нащадків $search_id з серіалізованної строки jsonString
     * jsonString- це серіалізована строка яку отримуємо з бази, відповідь поверне у вигляді масиву
     * приймає 2 параметра айдішку батьків які потрібно знати та несформатований jsonList, третій параметр     це маркер який вказує включати в масив айдішку яку шукають чи ні
     * повертає масив з дітьми
     * @param $search_id
     * @param $jsonString
     * @param bool $id
     * @return array
     */
    public static function findChildrenFromJsonString($search_id, $jsonString, $id = true){

        $childrens = self::childrenEach($search_id, self::convertToArray(json_decode($jsonString)));
        if($id) $childrens[] = $search_id;


        return $childrens;
    }

    /**
     * внутрішній клас перебирае масиви працює в методі children
     * @param $search_id
     * @param null $treeArray
     * @param array $children
     * @param null $marker
     * @return array
     */
    private static function childrenEach($search_id, $treeArray = null, &$children = [], $marker = null){
        foreach($treeArray as $key => $value){
            if($key == $search_id || $marker){
                if(is_array($value)) self::childrenEach($search_id, $value, $children, true);
            }
            else if(is_array($value)) self::childrenEach($search_id, $value, $children);
            if($marker) $children[] = $key;
        }
        return $children;
    }

    /** Поверне всіх нащадків $search_id з  $jsonList  у вигляді простого масиву
     * приймає 2 параметра айдішку батьків $search_id які потрібно знати та несформатований jsonList, третій параметр     це маркер який вказує включати в масив айдішку яку шукають чи ні
     * повертає масив з нащадками $search_id
     * @param $search_id
     * @param $jsonList
     * @param bool $id
     * @return array
     */
    public static function findChildren($search_id, $jsonList, $id = true){

        $childrens = self::childrenEach($search_id, self::convertToArray($jsonList));
        if($id) $childrens[] = $search_id;

        return $childrens;
    }

    /**Поверне  сереалізовану строку jsonList зі вставленою айдішкою
     * @param $jsonString //серіалізований json куди потріьно додати елемент
     * @param $id //айдішка яку потрібно додати в json
     * @return string //
     */
    public static function addItem($jsonString, $id){

        if(!$ar1 = json_decode($jsonString, true)) $ar1 = [];
        $ar2 = [];
        $ar2[0] = ['id' => $id];
        return json_encode(array_merge($ar1, $ar2));
    }

}
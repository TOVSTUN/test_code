<?php
/**
 * Created by PhpStorm.
 * User: tovstun
 * Date: 12.04.19
 * Time: 11:30
 * Докуметація  до бібліотеки лежить тут https://phpspreadsheet.readthedocs.io/en/stable/topics/accessing-cells/
 *
 * інтеграція
 *
 * $table = new MyExcelReader(Yii::getAlias('@webroot') . '/test.xls');
 * print_r($table->getLegend());
 * exit;
 * інсталяція  composer phpoffice/phpspreadsheet
 */

namespace common\helpers;


use phpDocumentor\Reflection\Types\This;
use PhpOffice\PhpSpreadsheet\IOFactory;
use yii\helpers\ArrayHelper;

/**
 * Class MyExcelReader
 * $file = new MyExcelReader($file_name);
 * print_r($file->getLegend()); exit;
 *
 * @package common\helpers
 */
class MyExcelReader {

    private $table;
    private $cells;
    private $activeSheet;

    public function __construct($file_name){

        $reader = IOFactory::createReaderForFile($file_name);
        $reader->setReadDataOnly(true);
        $this->table = $reader->load($file_name);
        $this->activeSheet = $this->table->getActiveSheet();
        $this->cells = $this->activeSheet->getCellCollection();
        // print_r($this->cells->getHighestRowAndColumn());exit; поверне Array ( [row] => 35 [column] => H )
        // print_r($this->cells->getHighestRow());exit; 35
        // print_r($this->cells->);
        // exit;
    }


    /**
     * функція поверне  одну строку по  її номеру з таблиці
     * @param integer $num_row хочу тут отримати номер ястроки яку потрібно вивести
     * @param string $withColuml номер колонки з якої потрібно почати працю
     * @param string $beforeColumn номер колонки до  якої потрібно виводити інформацію
     * @return array
     */
    public function getOneRow($num_row, $withColuml = 'A', $beforeColumn = null){

        if(!$beforeColumn) $beforeColumn = $this->cells->getHighestColumn(); // отримати максимальне значення стовпців
        $a = $this->activeSheet->rangeToArray($withColuml . $num_row . ':' . $beforeColumn . $num_row);// отримати ячейку по координатам
        return $a[0];
    }

    /**
     * метод поверне всю таблицю у вигляді масисиву (без легенди)
     */
    public function getAllRows(){

        $table = [];
        for($row = 2; $row <= $this->getCountRows(); $row++){
            $table[] = $this->getOneRow($row);
        }
        return $table;
    }

    /**
     * метод поверне легенду (назви колонок у вигляді массиву)
     */
    public function getLegend(){
        return $this->getOneRow(1);
    }

    /**
     * метод поверне колонку масиву, по її назві або по її номеру
     */
    public function getColumn($column_name){

        $legend = array_map('strtolower', $this->getLegend());

        if(!is_numeric($column_name)){
            $column_name = array_search(strtolower($column_name), $legend);
        }

        return ArrayHelper::getColumn($this->getAllRows(), $column_name);
    }


///==========PRIVARE=====================

    /**
     * отримати кількість строк на цій таблиці
     */
    private function getCountRows(){

        return $this->cells->getHighestRow();
    }
}



/*акже возможно , чтобі получить диапазон значений ячеек в массив в одном візове , используя toArray(), rangeToArray()или  namedRangeToArray()методі.

$dataArray = $spreadsheet->getActiveSheet()
    ->rangeToArray(
        'C3:E5',     // The worksheet range that we want to retrieve
        NULL,        // Value that should be returned for empty cells
        TRUE,        // Should formulas be calculated (the equivalent of getCalculatedValue() for each cell)
        TRUE,        // Should values be formatted (the equivalent of getFormattedValue() for each cell)
        TRUE         // Should the array be indexed by cell row and cell column
    );
Все єти методі возвращают двумерній массив строк и столбцов. toArray()Метод будет возвращать весь рабочий лист; rangeToArray() вернет указанній диапазон или ячейки; while namedRangeToArray()вернет ячейки в пределах определенного named range.*/
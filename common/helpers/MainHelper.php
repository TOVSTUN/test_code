<?php
/**
 * Created by PhpStorm.
 * User: tovstun
 * Date: 14.11.18
 * Time: 9:09
 *
 *
 */

/* @var $this yii\web\View */

namespace common\helpers;

use common\entities\Company;
use Psr\Log\NullLogger;
use Yii;
use yii\base\ExitException;
use yii\helpers\Url;

class MainHelper {


    /**Цей Метод поверне обєкт класу Company,
     * якщо в get параметрі буде айдішка Активної (це компанія зі статусом 10) компанії (company_id),
     * інакше редиректне на сторінку вибору компаній
     * Увана отримати обєкт неактивної компанії неможливо
     * @return Company
     * @throws \yii\base\ExitException
     */
    public static function getCurrentCompany(): Company{

        $id = \Yii::$app->request->get('company_id') ?? null;

        if(!$company = Company::findOne($id)){

            self::setFlash('Firmy z takim identyfikatorem nie istnieje');
            Yii::$app->getResponse()->redirect('/companies')->send();
            Yii::$app->end();
        }

        if($company->status == 1){

            self::setFlash('Przepraszamy, ale firma <strong>' . $company->name . '</strong> nie jest skonfigurowana, dodaj cennik oraz grupu towarową');
            Yii::$app->getResponse()->redirect('/companies')->send();
            Yii::$app->end();
        }

        return $company;

    }

    public static function getCurrentCompanyId(){

        $a = Yii::$app->request->get('company_id');
        return $a ?? null;
    }

    public static function getCurrentCompanyByUrl(){
        $company = false;
        $domain = explode('/', preg_replace('#^https?://#', '', Url::current([], true)));
        //  print_r(Yii::$app->db->dsn); exit;

        if($domain[0] == 'mor.loc' && Yii::$app->db->dsn == 'mysql:host=1000.69.100.250;dbname=mor'){
            $company = Company::find()->where(['id' => 32525])->one();

        }
        elseif($domain[0] == 'test.pl' && Yii::$app->db->dsn == 'mysql:host=100.69.100.250;dbname=mor'){
            $company = Company::find()->where(['id' => 32525])->one();

        }
        else{
            if(!$company = Company::find()->where(['domain' => $domain[0]])->one()){

                self::setFlash('Firmy z takim identyfikatorem nie istnieje');
                Yii::$app->getResponse()->redirect('/login')->send();
                Yii::$app->end();
            }
        }


        if(!$company && $company->status == 1){

            self::setFlash('Przepraszamy, ale firma <strong>' . $company->name . '</strong> nie jest skonfigurowana');
            Yii::$app->getResponse()->redirect('/login')->send();
            Yii::$app->end();
        }

        return $company;
    }

    private static function setFlash($info){

        Yii::$app->session->setFlash('danger', $info);

    }

    public static function decodeTime_Y_M_D($date){
        //print_r($date); exit;

        if($date && strlen($date) > 5){
            list($year, $month, $day) = explode('-', $date);
            return strtotime($day . '-' . $month . '-' . $year);
        }
        return 0;
    }
}
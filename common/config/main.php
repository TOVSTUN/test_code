<?php
return [
    'aliases'    => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache'     => [
            'class' => 'yii\caching\FileCache',
        ],
        'settings'  => [
            'class'            => 'common\components\MainSettings',
            'default_language' => 'pl',
        ],
        'formatter' => [
            'class'      => 'yii\i18n\Formatter',
            'dateFormat' => 'd.MM.yyyy',
        ],
        /*'cache' => [
            'class' => 'yii\caching\MemCache',
            'servers' => [
                [
                    'host' => 'localhost',
                    'port' => 11211,
                ],
            ],
            'useMemcached' => true,
        ],*/
    ],
];

<?php
/**
 * Created by PhpStorm.
 * User: tovstun
 * Date: 02.02.18
 * Time: 0:11
 */

namespace common\statuses;


class MyStatus {


    const STATUS_ACTIVE = 10;
    const STATUS_INACTIVE = 0;

    public static function onOff(){

        return
            [
                self::STATUS_ACTIVE   => \Yii::t('app', 'Aktywny'),
                self::STATUS_INACTIVE => \Yii::t('app', 'Zablokowany'),
            ];
    }

    public static function statusDoc(){

        return
            [
                self::STATUS_ACTIVE   => \Yii::t('app', 'Otwarta'),
                self::STATUS_INACTIVE => \Yii::t('app', 'Zakończona'),
            ];
    }

    public static function noYes(){

        return
            [
                self::STATUS_INACTIVE => \Yii::t('app', 'Nie'),
                self::STATUS_ACTIVE   => \Yii::t('app', 'Tak'),
            ];
    }

    public static function whatStatus($status){
        $arr = self::onOff();
        return $arr[$status];
    }

    public static function whatStatusDoc($status){
        $arr = self::statusDoc();
        return $arr[$status];
    }


    public static function whatJak($status){
        $arr = self::noYes();
        return $arr[$status] ?? $arr[0];
    }

}